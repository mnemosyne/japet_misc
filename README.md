# japet_misc library

## Description

Some python utils on: list, dic, plots, ....

## Installation

### from gitlab

```
pip install git+https://gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/japet_misc.git
```

Ou

```
pip install git+shh://git@gricad-gitlab.univ-grenoble-alpes.fr:mnemosyne/japet_misc.git
```

### test install

Get install directory
```
pip show japet_misc
```

Use pytest on japet_misc path
```
pytest --doctest-modules /path_wherre_pip_install/site-packages/japet_misc
```

## Usage


```

```

## License
GNU GPL

