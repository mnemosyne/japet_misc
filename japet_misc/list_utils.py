"""
Python Module for operation on lists
"""

##======================================================================================================================##
##                                PACKAGE                                                                               ##
##======================================================================================================================##

import argparse
import doctest
import bisect
from collections.abc import Iterable
from numbers import Number

import numpy

from japet_misc.numb_utils import normalize_numb, norm_numb

##======================================================================================================================##
##                                LIST FUNCTIONS                                                                        ##
##======================================================================================================================##

def all_linear_list_in_a_vector(vec_len, n_side_remove=None, break_lens='all'):
    """
    >>> d = all_linear_list_in_a_vector(4)
    >>> for i in d[2]: print(list(i))
    [0.0, 1.0, 1.0, 1.0]
    [0.0, 0.0, 1.0, 1.0]
    [0.0, 0.0, 0.0, 1.0]
    >>> for i in d[3]:print(list(i))
    [0.0, 0.5, 1.0, 1.0]
    [0.0, 0.0, 0.5, 1.0]
    >>> (d[4] == numpy.array([[0, 1/3., 2/3., 1]])).all()
    True
    >>> d = all_linear_list_in_a_vector(4, 1)
    >>> len(d[2])
    1
    >>> all(d[2][0] == numpy.array([0., 0., 1., 1.]))
    True
    >>> for i in d[3]:print(list(i))
    [0.0, 0.5, 1.0, 1.0]
    [0.0, 0.0, 0.5, 1.0]
    >>> (d[4] == numpy.array([[0, 1/3., 2/3., 1]])).all()
    True
    """
    assert isinstance(vec_len, Number), 'Input: exp numb, got : {0.__class__} : {0}'.format(vec_len)
    assert isinstance(n_side_remove, Number) or n_side_remove is None, 'Input(exp numb) got {0}'.format(n_side_remove)
    assert isinstance(break_lens, Iterable) or break_lens == 'all', 'Exp:list or "all" got : {0}'.format(break_lens)

    res = {}

    if n_side_remove is None:
        n_side_remove = 0

    assert n_side_remove * 2 <= vec_len, 'on ne vas retirer de fois plus de la moitie'

    if break_lens == 'all':
        break_lens = range(2, vec_len + 1) #boucle entre 2 et vec_len +1 pour avoir [2 ... vec_len] arg of linear_list

    assert min(break_lens) >= 2, 'min val of break_lens must be 2 or more : got {0}'.format(min(break_lens))
    assert max(break_lens) <= vec_len, 'breaklens maxval must be < veclen got {0} vs {1}'.format(max(break_lens), vec_len)

    for break_len in break_lens:

        alls = []
        linlist = linear_list(break_len)  #list correspondant a la longueur du break --> linear_list(2) = [0, 1]

        #+2 car breaklencommence a 2 donc -2 + 2 = 0 pour un break de deux on ne prend pas n_side_remove
        side_rm = n_side_remove - break_len + 2

        # ~ if side_rm <= 0:
            # ~ side_rm = 0
        side_rm = max(side_rm, 0)


        add1 = [0 for i in range(side_rm)] #list a ajouter au debut correspondant a ce quon autorise pas avec side_rm
        add2 = [1 for i in range(side_rm)] #idem mais a la fin

        restant = vec_len - break_len - 2* side_rm #Nb of cases restantes e milieu de vecteur pour faire passer le break

        #il y a 1 posibilite de plus que le nombre de case restantes : 
        #sil y a 1 case restante il y 2 possibilite pour mettre linlist [0] + linlist et linlist + [1]
        for istart in range(restant + 1): 

            to_add = numpy.array(add1 + [0 for i1 in range(istart)] + linlist + [1 for i2 in range(restant - istart)] + add2, dtype=float)
            assert to_add.size == vec_len, 'pb code source'
            alls.append(to_add)

        if alls: #list contains some values
            res[break_len] = alls

    return res

def compare_2_numpy_dic(dic1, dic2):
    """
    >>> d1={k: numpy.arange(k) for k in range(1,10)}
    >>> d2={k: numpy.arange(k) for k in range(1,10)}
    >>> d3={k: numpy.arange(k) + 1 for k in range(1,10)}
    >>> d4={k: numpy.arange(k) for k in range(1,11)}
    
    >>> compare_2_numpy_dic(d1,d2)
    True
    >>> compare_2_numpy_dic(d1,d3)
    False
    >>> compare_2_numpy_dic(d1,d4)
    False
    """
    res = True
    
    keys1 = sorted(dic1.keys())
    keys2 = sorted(dic2.keys())
    
    if keys1 == keys2:
        for k in keys1:
            if all(dic1[k] == dic2[k]):
                pass
            else:
                res = False
    else:
        res = False
    
    return res


def linear_list(length):
    """
    Retourne une liste lineaire entre 0 et 1 de longueur n
    >>> linear_list(1)
    Traceback (most recent call last):
            ...
    AssertionError: le nombre delement renvoyer est forcement superieur a deux : [0, 1] got 1
    >>> linear_list(2) == [0, 1]
    True
    >>> linear_list(3) == [0, 0.5, 1]
    True
    >>> linear_list(5) == [0, 0.25, 0.5, 0.75, 1]
    True
    """
    assert isinstance(length, Number), 'pb entree nombre attendu got {0.__class__} : {0}'.format(length)
    assert length > 1, 'le nombre delement renvoyer est forcement superieur a deux : [0, 1] got {0}'.format(length)

    #~ frac = sympy.Rational(1, length - 1).evalf()
    frac = 1 / (length - 1)

    res = [frac * i for i in range(length)]
    end = res[-1]
    assert abs(end - 1) < 1e-8, "pb code source dernier element different de 1 : {0}".format(end)
    res[-1] = 1.0
    return res
    
def linvector(start, end, sep=1, add_end=True, dtype=float):
    """
    Function doc
    >>> all(linvector(0, 1.1, 0.5) == numpy.array([0. , 0.5, 1. , 1.1]))
    True
    >>> all(linvector(0, 1.1, sep = 0.5) == numpy.array([0. , 0.5, 1. , 1.1]))
    True
    >>> all(linvector(0, 1, sep = 0.5) == numpy.array([0. , 0.5, 1. ]))
    True
    >>> all(linvector(*(0, 1.1), sep = 0.5) == numpy.array([0. , 0.5, 1. , 1.1]))
    True
    >>> all(linvector(*(0, 1),sep= 0.5) == numpy.array([0. , 0.5, 1. ]))
    True
    >>> all(linvector(*(0, 1, 0.5)) == numpy.array([0. , 0.5, 1. ]))
    True
    """
    assert isinstance(sep, Number), 'pb exp {0} got {1.__class__} : {1}'.format("numb", sep)
    assert isinstance(start, Number), 'pb exp {0} got {1.__class__} : {1}'.format("numb", start)
    assert isinstance(end, Number), 'pb exp {0} got {1.__class__} : {1}'.format("numb", end)
    
    dist = end - start
    n = int(dist / sep) + 1
    res = [start + i * sep for i in range(n)]
    if res[-1] != end and add_end:
        res.append(end)

    res = numpy.array(res, dtype=dtype)
    return res

def reduce_list(vec, typ=list):
    """
    >>> reduce_list([1, 2])
    [1, 2]
    >>> reduce_list([[1, 2]])
    [1, 2]
    >>> all(reduce_list([[1, 2]], numpy.array) == numpy.array([1, 2]))
    True
    >>> reduce_list([1])
    1
    >>> reduce_list([[[1]]])
    1
    >>> reduce_list([[['bonjour', 1]]])
    ['bonjour', 1]
    """
    assert isinstance(vec, Iterable), "veuillez entrer un Iterable %s est dui type %s"% (vec, type(vec))
    assert typ in (list, tuple, numpy.array), "type doit etre numpy, list ou tupple"

    vec = list(vec)
    if len(vec) == 1:
        res = vec[0]
        if isinstance(res, Iterable) and not isinstance(res, str):
            res = reduce_list(res, typ)
    else:
        res = typ(vec)

    return res

def reduce_list_if_same_value(vec, recursive_on_list=False, force_list=False):
    """
    >>> reduce_list_if_same_value([0, 0])
    0
    >>> reduce_list_if_same_value([0, 0], force_list = True)
    [0]
    >>> reduce_list_if_same_value([[0, 0]])
    [0, 0]
    >>> reduce_list_if_same_value([[0, 0], 0], True)
    0
    >>> reduce_list_if_same_value([[0, 0], 0], True, force_list = True)
    [0]
    >>> reduce_list_if_same_value([[0, 0], [0, 1, 2]])
    [[0, 0], [0, 1, 2]]
    >>> reduce_list_if_same_value([[0, 0], [0, 1, 2]], True)
    [0, 1, 2]
    >>> reduce_list_if_same_value([1, 2, 3, [0, 0], [0, 1, 2]], True)
    [0, 1, 2, 3]
    >>> reduce_list_if_same_value([1, 5])
    [1, 5]
    >>> reduce_list_if_same_value([5, 2, 1, 1, 2, 5, 5])
    [1, 2, 5]
    >>> reduce_list_if_same_value([1, 1, 2, 5, "a"])
    [1, 2, 5, 'a']
    """
    assert isinstance(vec, Iterable), "List expected, got :{0.__class__}\n{0}".format(vec)
    
    v0 = vec[0]
    
    if recursive_on_list and isinstance(v0, Iterable):
        v0 = reduce_list_if_same_value(v0, recursive_on_list=True)
    
    res = [v0]
    
    for v in vec:
        if recursive_on_list and isinstance(v, Iterable):
            vals = res + v
            res = reduce_list_if_same_value(vals, recursive_on_list=True)
            if not isinstance(res, Iterable):
                res = [res]
        else:
            newval = v not in res
            if newval:
                res.append(v)
                    
    if len(res) == 1 and not force_list:
        res = res[0]
        #~ if type(res) in Iterable:
                #~ res = reduce_list_if_same_value(res)
    else:
        if all_same_type_in_list(res):
            res = sorted(res)
        
    return res

def all_same_type_in_list(vec):
    """
    >>> all_same_type_in_list([i for i in range(10)])
    True
    >>> all_same_type_in_list([i * 'str' for i in range(10)])
    True
    >>> all_same_type_in_list([[i] for i in range(10)])
    True
    >>> all_same_type_in_list([i for i in range(10)] + ['str'])
    False
    """
    t0 = type(vec[0])
    res = all(isinstance(vi, t0) for vi in vec)
            
    return res
        

def flat_list(vec):
    """
    >>> flat_list(list(range(5)))
    [0, 1, 2, 3, 4]
    >>> vec = [("A", "B") for i in range(3)]
    >>> vec
    [('A', 'B'), ('A', 'B'), ('A', 'B')]
    >>> flat_list(vec)
    ['A', 'B', 'A', 'B', 'A', 'B']
    >>> vec[1] = list(range(5)) + ["C", ("D", "E")]
    >>> vec
    [('A', 'B'), [0, 1, 2, 3, 4, 'C', ('D', 'E')], ('A', 'B')]
    >>> flat_list(vec)
    ['A', 'B', 0, 1, 2, 3, 4, 'C', 'D', 'E', 'A', 'B']
    >>> vec[2] = [["C", ("D", "E")] + ["F", "G", ("O", "P", ["U", "V"])], ("X", "Y")]   
    >>> vec
    [('A', 'B'), [0, 1, 2, 3, 4, 'C', ('D', 'E')], [['C', ('D', 'E'), 'F', 'G', ('O', 'P', ['U', 'V'])], ('X', 'Y')]]
    >>> flat_list(vec)
    ['A', 'B', 0, 1, 2, 3, 4, 'C', 'D', 'E', 'C', 'D', 'E', 'F', 'G', 'O', 'P', 'U', 'V', 'X', 'Y']
    """
    assert isinstance(vec, Iterable), "'list' required got {0}".format(vec)
    res = []
    for i in vec:
        if isinstance(i, Iterable) and not isinstance(i, str):
            res += list(i)
        else:
            res.append(i)
    
    if any(isinstance(i, Iterable) and not isinstance(i, str) for i in res):
        res = flat_list(res)
    
    return res

        
def sort_list(vec):
    """
    >>> sort_list([3, 5, 7, 6, 1]) == [1,3,5,6,7]
    True
    >>> sort_list([3, '5', 7, '6', 1]) == [1,3,'5','6',7]
    True
    >>> sort_list([3, '5b', 5, 'a', 1]) ==[1, 3, 5, '5b', 'a']
    True
    >>> sort_list([3, '5b', 5, 'a', 1, 5]) ==[1, 3, 5, 5, '5b', 'a']
    True
    """
    tosort = [str(i) for i in vec]
    indexes = numpy.argsort(tosort)
    res = numpy.array(vec, dtype=object)[indexes]
    res = list(res)
    return res

def intersection(*arg):
    """
    >>> intersection((list(range(10)), list(range(-5, 5))))
    [0, 1, 2, 3, 4]
    >>> intersection(list(range(10)), list(range(-5, 5)))
    [0, 1, 2, 3, 4]
    """
    assert arg, "pb entree : {0} attendu {1}".format("arg", arg)
    
    if len(arg) == 1:
        vecs = arg[0]
    else:
        vecs = arg
            
    assert isinstance(vecs, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format("list", vecs)
    assert all(isinstance(v, Iterable) for v in vecs), 'Input, exp {0} got {1.__class__} : {1}'.format("list", vecs)
    
    v0 = list(vecs[0])
    res = set(v0)
    
    for ivec in vecs:
        ivec = list(ivec)
        res = res.intersection(ivec)
            
    res = sorted(res)
    return res

def union(*arg):
    """
    >>> union((list(range(10)), list(range(-5, 5))))
    [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    >>> union(list(range(10)), list(range(-5, 5)))
    [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    """
    assert arg, "pb entree : {0} attendu {1}".format("arg", arg)
    
    if len(arg) == 1:
        vecs = arg[0]
    else:
        vecs = arg
    
    assert isinstance(vecs, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format("list", vecs)
    assert all([isinstance(v, Iterable) for v in vecs]), 'Input exp {0} got {1.__class__} : {1}'.format("list", vecs)
    
    res = []
    for v in vecs:
        res += list(v)
    
    res = sorted(set(res))
    return res
        
def index_list_for_a_given_size(ntot, n_new):
    """
    >>> all(index_list_for_a_given_size(10,5) == numpy.array([0, 2, 4, 7, 9]))
    True
    >>> all(index_list_for_a_given_size(10,6) ==numpy.array([0, 2, 4, 5, 7, 9]))
    True
    >>> all(index_list_for_a_given_size(15,6) == numpy.array([ 0,  3,  6,  8, 11, 14]))
    True
    """
    assert ntot > n_new, "ntot is expected to be higher than n_new"
    ind = numpy.linspace(start=0, stop=ntot - 1, num=n_new, endpoint=True)
    res = numpy.round(ind)
    res = numpy.array(res, dtype=int)
    return res

def reduce_list_to_a_given_size(vec, n_new):
    """
    >>> vec = [0] + list(numpy.arange(10,40,2)) + [50]
    >>> vec
    [0, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 50]
    >>> len(vec)
    17
    >>> all(reduce_list_to_a_given_size(vec, 10) == numpy.array([ 0, 12, 16, 18, 22, 26, 30, 32, 36, 50]))
    True
    >>> all(reduce_list_to_a_given_size(vec, 7) == numpy.array([ 0, 14, 18, 24, 30, 34, 50]))
    True
    >>> all(reduce_list_to_a_given_size(vec, 5) == numpy.array([ 0, 16, 24, 32, 50]))
    True
    """
    ntot = len(vec)
    vec = numpy.array(vec)
    assert n_new < ntot, "can not reduce to a larger number"
    index = index_list_for_a_given_size(ntot, n_new)
    res = vec[index]
    
    return res 
        
def split_vector_indexes(total_size, size1, method="simple"):
    """
    >>> all(split_vector_indexes(10, 3, "simple")["ind1"] == numpy.array([0, 1, 2]))
    True
    >>> all(split_vector_indexes(10, 5, "supervised")["ind1"] == numpy.array([1, 3, 5, 7, 9]))
    True
    """
    indexes = numpy.arange(total_size)
    if method == "simple":
        ind1 = indexes[:size1]
        ind2 = indexes[size1:]
    elif method == "supervised":
        size2 = total_size - size1
        ratio = size1 / size2
        
        ind1 = []
        ind2 = []
        
        for i in indexes:
            rest = i % (ratio + 1)
            if rest == 0:
                ind2.append(i)
            else:
                ind1.append(i)
        ind1 = numpy.array(ind1, dtype=int)
        ind2 = numpy.array(ind2, dtype=int)
    else:
        assert 0, "not implemented"
            
    res = {"ind1" : ind1, "ind2":ind2}
    return res
        
        
def normalize_list(vec, factor=None):
    """
    >>> normalize_list([-5, 15], 1e1)
    (-10, 20)
    >>> normalize_list([-5, 15], 2e1)
    (-20, 20)
    >>> normalize_list([-5, 15], 1e2)
    (-100, 100)
    >>> normalize_list([-5, 15], None)
    (-5, 15)
    >>> normalize_list([-0.1, 150.5], None) == (-0.1, 150.5)
    True
    """
    assert isinstance(vec, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format("listtype", vec)

    minval = min(vec)
    maxval = max(vec)

    if factor is not None:
        minval = normalize_numb(minval, "min", factor=factor)
        maxval = normalize_numb(maxval, "max", factor=factor)

    res = (minval, maxval)
    return res



def minmax_tupple_from_values(values, factor=None):
    """
    >>> minmax_tupple_from_values(numpy.arange(-5, 10), 1)
    (-9, 9)
    >>> minmax_tupple_from_values(numpy.arange(-5, 10), 10)
    (-10, 10)
    >>> minmax_tupple_from_values([-0.9, -1.5, -1.6, -2.5], 0.1)
    (-2.5, 2.5)
    >>> minmax_tupple_from_values([-0.9, -1.5, -1.6, -2.5], None)
    (-2.5, 2.5)
    >>> minmax_tupple_from_values([-0.9, -1.5, -1.6, -2.6], None) == (-2.6, 2.6)
    True

    """
    assert isinstance(values, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format("list", values)
    maxval = numpy.abs(values).max()
    if factor is not None:
        maxval = normalize_numb(numb=maxval, born="max", factor=factor)
    res = (- maxval, maxval)
    return res
        
def all_nan(array):
    """
    >>> vec = numpy.arange(100.)
    >>> all_nan(vec)
    False
    >>> vec[0] = numpy.nan
    >>> all_nan(vec)
    False
    >>> vec = [numpy.nan for i in vec]
    >>> all_nan(vec)
    True
    >>> vec[0] = 1
    >>> all_nan(vec)
    False
    >>> vec = numpy.arange(100).reshape(10,10)
    >>> all_nan(vec)
    False
    >>> vec = numpy.array([[numpy.nan for i in range(10)] for j in range(10)]).reshape(10,10)
    >>> all_nan(vec)
    True
    >>> vec[0,0] = 1
    >>> all_nan(vec)
    False
    >>> vec = numpy.array([[[numpy.nan for i in range(10)] for j in range(10)] for k in range(2)])
    >>> all_nan(vec)
    True
    >>> vec[0,0, -1] = 1
    >>> all_nan(vec)
    False
    """
    assert isinstance(array, Iterable), 'pb input : expected {0} got {1.__class__} : {1}'.format("list type", array)
    array = numpy.array(array)
    shape = array.shape
    if len(shape) > 1:
        array = flat_list(array)
        array = numpy.array(array)

    #count the number of nan in the array
    n_nan = numpy.isnan(array).sum()
    #compare it to the total size of the array
    size = array.size
    res = size == n_nan
    return res

        
def to_valid_indexes(indexes):
    """
    >>> to_valid_indexes(1)
    array([1])
    >>> to_valid_indexes([1])
    array([1])
    >>> to_valid_indexes(numpy.array(1))
    array([1])
    >>> to_valid_indexes([1, 4])
    array([1, 4])
    """
    if isinstance(indexes, int):
        res = [indexes]
    else:
        res = indexes
    
    if isinstance(res, (list, tuple)):
        res = numpy.array(res, dtype=int)
    else:
        assert isinstance(res, numpy.ndarray), "pb input : {0}".format(res)
            
    if not numpy.shape(res):
        res = numpy.array([int(res)])
    
    assert len(numpy.shape(res)) == 1 and numpy.size(res) > 0, "pb output : {0}".format(res)
    
    return res
        
def dicind_to_valid_dicind(dicind):
    """
    >>> dicind_to_valid_dicind({'indy': [0], "indx" : [0]}) == {'indx': numpy.array([0]), 'indy': numpy.array([0])}
    True
    >>> dicind_to_valid_dicind({'indy': numpy.array(0), "indx" : 0})=={'indx': numpy.array([0]), 'indy': numpy.array([0])}
    True
    """
    assert isinstance(dicind, dict), "pb input : must be dic"
    assert "indy" in dicind and "indx" in dicind, "indx and indy must be there"

    res = {kind: to_valid_indexes(vind) for kind, vind in dicind.items()}
    
    return res
        
        
def check_indexes_shape(dicind):
    """
    >>> check_indexes_shape({'indy': [0], "indx" : [0]})
    >>> check_indexes_shape({'indy': [0], "indx" : [0, 2]})
    Traceback (most recent call last):
            ...
    AssertionError: indx and indy must have same size
    >>> check_indexes_shape(extend_indexes({'indy': [0], "indx" : [0, 2]}))
    """
    assert numpy.size(dicind["indy"]) == numpy.size(dicind["indx"]), "indx and indy must have same size"
    assert numpy.shape(dicind["indy"]) == numpy.shape(dicind["indx"]), "indx and indy must have same shape"
    
    return None
        
def extend_indexes(dicind):
    """
    >>> inds = extend_indexes({'indy': [0], "indx" : [0]})
    >>> inds["indx"]
    array([0])
    >>> inds["indy"]
    array([0])
    >>> inds = extend_indexes({'indy': [0], "indx" : [0,1,2]})
    >>> inds["indy"]
    array([0, 0, 0])
    >>> inds["indx"]
    array([0, 1, 2])
    >>> inds = extend_indexes({'indy': [0,10], "indx" : [0,1,2]})
    >>> inds["indx"]
    array([0, 1, 2, 0, 1, 2])
    >>> inds["indy"]
    array([ 0,  0,  0, 10, 10, 10])
    """
    dicind = dicind_to_valid_dicind(dicind)
    
    indx = dicind['indx']
    indy = dicind['indy']

    nx = indx.size
    ny = indy.size
    
    indxs = [indx[ix] for iy in range(ny) for ix in range(nx)]
    indys = [indy[iy] for iy in range(ny) for ix in range(nx)]
    res = {"indx": indxs, 'indy' : indys}
    res = dicind_to_valid_dicind(res)
    
    return res
        
def transform_indexes(dicind, extended=False, fmt=dict):
    """
    >>> transform_indexes({'indy': [0], "indx" : [1]}, False, fmt = tuple)
    (array([0]), array([1]))
    >>> transform_indexes({'indy': [0, 10], "indx" : [1, 2, 3]}, False, fmt = tuple)
    (array([ 0, 10]), array([1, 2, 3]))
    >>> transform_indexes({'indy': [0, 10], "indx" : [1, 2, 3]}, True, fmt = tuple)
    (array([ 0,  0,  0, 10, 10, 10]), array([1, 2, 3, 1, 2, 3]))
    >>> transform_indexes({'indy': [0, 10], "indx" : [1, 2, 3]}, False, fmt = list)
    Traceback (most recent call last):
            ...
    AssertionError: indx and indy must have same size
    >>> exp = [{'indx': 1,'indy': 0},{'indx': 2,'indy': 0},{'indx': 3,'indy': 0},{'indx':1,'indy': 10},{'indx': 2,'indy':10},{'indx': 3, 'indy': 10}]
    >>> transform_indexes({'indy': [0, 10], "indx" : [1, 2, 3]}, True, fmt = list) == exp
    True
    """
    assert isinstance(dicind, dict), "pb input : must be dic"
    assert "indy" in dicind and "indx" in dicind, "indx and indy must be there"
    
    if extended:
        dicind = extend_indexes(dicind)
    else:
        dicind = dicind_to_valid_dicind(dicind)
    
    if fmt == dict:
        res = dicind.copy()
    elif fmt == tuple:
        res = (dicind["indy"], dicind['indx'])
    elif fmt == list:
        check_indexes_shape(dicind)
        n = dicind['indx'].size
        res = [{k_ind : v_ind[iii] for k_ind, v_ind in dicind.items()} for iii in range(n)]
    else:
        assert 0, "{0} fmt not implemented"
    
    return res


def slice_vector(vec, start, end, condition="leq-geq"):
    """
    Function doc
    >>> vec = numpy.arange(10)
    >>> all(vec[slice_vector(vec, -1, 12)] == numpy.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]))
    True
    >>> all(vec[slice_vector(vec, 0, 9)] == numpy.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]))
    True
    >>> all(vec[slice_vector(vec, 1, 9)] == numpy.array([1, 2, 3, 4, 5, 6, 7, 8, 9]))
    True
    >>> all(vec[slice_vector(vec, -1, 8)] == numpy.array([0, 1, 2, 3, 4, 5, 6, 7, 8]))
    True
    >>> all(vec[slice_vector(vec, 4, 5)] == numpy.array([4, 5]))
    True
    >>> all(vec[slice_vector(vec, 2.5, 8.9)] == numpy.array([3, 4, 5, 6, 7, 8]))
    True
    """
    assert condition == "leq-geq", "not implemented : {0}".format(condition)
    assert end > start, "pb input end must be > start :  got\nstart = {0}\nend = {1}".format(start, end)
    
    i1 = bisect.bisect_left(vec, start)
    i2 = bisect.bisect_right(vec, end)
    
    res = slice(i1, i2, 1)

    #checking
    i_first = res.start    #first selected index
    i_last = res.stop - 1 #last selected index

    i_before = i_first - 1 # index before selected indexes
    i_after = i_last + 1  # index after selected indexes
    
    #start check
    #~ print(i_f, i1, i_a, i2)
    
    
    assert i_first == 0 or vec[i_before] < start, "pb code"        #first selected or last not selected pt before first elmt
    assert vec[i_first] >= start, 'pb_code'                        #first selected > start
    
    assert i_last == len(vec) - 1 or vec[i_after] > end, 'pb' #last date or first not seletcted after end
    assert vec[i_last] <= end, "pb code"                      #last selected lower than end
    
    return res


##======================================================================================================================##
##                                BOUNDARIES                                                                            ##
##======================================================================================================================##

def extend_bds(bds, value=numpy.inf):
    """
    Add -inf and inf to the list
    >>> all(extend_bds([0,2]) == numpy.array([-numpy.inf,   0.,   2.,  numpy.inf]))
    True
    >>> all(extend_bds(numpy.arange(6)) == numpy.array([-numpy.inf,   0.,   1.,   2.,   3.,   4.,   5.,  numpy.inf]))
    True
    """
    if value == numpy.inf:
        pass
    elif value is None:
        value = max(bds) - min(bds)
    else:
        assert isinstance(value, Number) and value > 0, f"exp number > 0, got {value}"

    bf = min(bds) - value
    af = max(bds) + value

    res = numpy.concatenate([[bf], bds, [af]])
    return res

def auto_bounds(values, nbd=10, adjust_limits=False):
    """
    >>> all(auto_bounds(numpy.arange(11, 111, 2), nbd = 5)== numpy.array([ 10.,  30.,  50.,  70.,  90., 100.]))
    True
    >>> all(auto_bounds(numpy.arange(9, 15.5, 1), nbd = 5)== numpy.array([ 9., 10., 11., 12., 13., 14., 15., 16., 17., 18., 19., 20.]))
    True
    >>> all(auto_bounds(numpy.arange(-6, 191, 10), nbd = 5 )== numpy.array([ -6.,  34.,  74., 114., 154., 194., 200.]))
    True
    >>> all(auto_bounds(numpy.arange(-6, 191, 10), nbd = 10 ) == numpy.array([ -6.,  14.,  34.,  54.,  74.,  94., 114., 134., 154., 174., 194.,200.]))
    True
    >>> all(auto_bounds(numpy.arange(-6, 191, 10), nbd = 5, adjust_limits = True ) ==  numpy.array([ -6.,  34.,  74., 114., 154.]))
    True
    >>> all(auto_bounds(numpy.arange(-6, 191, 10), nbd = 10, adjust_limits = True ) == numpy.array([ -6.,  14.,  34.,  54.,  74.,  94., 114., 134., 154., 174.]))
    True
    >>> all(auto_bounds([260,275,280])== numpy.array([260., 262., 264., 266., 268., 270., 272., 274., 276., 278., 280.]))
    True
    """
    mn, mx = normalize_list(values)
    totsep = mx - mn
    sep = totsep / nbd
    
    #~ sep = norm_numb(sep, rnd)
    #~ print(sep)
    args = (mn, mx, sep)
    
    #~ fguess = 
    res = []
    #~ print(fguess)
    iiter = 1
    while len(res) <= nbd and iiter <= 10:
        rnd = ".{0}g".format(iiter)
        news = norm_numb(args, rnd)
        res = linvector(*news, add_end=True)
        iiter += 1
        #~ print(rnd)
    
    if adjust_limits:
        mn = min(values)
        mx = max(values)
        ind = (res >= mn) & (res <= mx)
        #~ print(res.size, ind.sum())
        res = res[ind]
    
    return res

def factor_bounds(vmin, vmax, typ="uniform", factor=2):
    """
    >>> all(factor_bounds(vmin = 1, vmax = 10, typ = "uniform", factor = 1)== numpy.array([ 1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9., 10.]))
    True
    >>> all(factor_bounds(vmin = 1, vmax = 10, typ = "linear", factor = 2)== numpy.array([ 1.,  2.,  4.,  8., 16.]))
    True
    >>> all(factor_bounds(vmin = 1, vmax = 10, typ = "linear", factor = 3)== numpy.array([ 1.,  3.,  9., 27.]))
    True
    >>> all(factor_bounds(vmin = 2, vmax = 100, typ = "power", factor = 2) == numpy.array([  2.,   4.,  16., 256.]))
    True
    """
    if typ == "uniform":
        res = linvector(vmin, vmax + factor/2, factor, add_end=False)
    else:
        assert factor > 1, 'pb input : expected {0} got {1.__class__} : {1}'.format("!=1", factor)

        res = [vmin]
        #~ cond = True
        i = 0
        while max(res) < vmax:
            old = res[-1]
            if typ == "power":
                new = old ** factor 
            elif typ == "linear":
                new = old * factor
            else:
                assert 0, f'{typ} : Not implemented'
            
            res.append(new)
            i += 1
            assert i < 1000, "infinite loop : job aborded"

    res = numpy.array(res, dtype=float)
        
    return res
        
def symmetric_bounds(bds, center=0, add_center=True):
    """
    >>> bds = factor_bounds(vmin = 1, vmax = 5, typ = "uniform", factor = 1)
    >>> all(symmetric_bounds(bds, center = 0, add_center = False) == numpy.array([-5., -4., -3., -2., -1.,  1.,  2.,  3.,  4.,  5.]))
    True
    >>> all(symmetric_bounds(bds, center = 0, add_center = True) == numpy.array([-5., -4., -3., -2., -1.,  0.,  1.,  2.,  3.,  4.,  5.]))
    True
    
    >>> bds = [1.25,2,5, 10]
    >>> all(symmetric_bounds(bds, center = 1, add_center = False) == numpy.array([ 0.1 ,  0.2 ,  0.5 ,  0.8 ,  1.25,  2.  ,  5.  , 10.  ]))
    True
    >>> all(symmetric_bounds(bds, center = 1, add_center = True) == numpy.array([ 0.1 ,  0.2 ,  0.5 ,  0.8 ,  1.  ,  1.25,  2.  ,  5.  , 10.  ]))
    True
    """
    bds = numpy.array(bds, dtype=float)
    if bds[0] == center:
        bds = bds[1:]
    
    if not add_center: #nombre pair
        toadd = []
    else:
        toadd = [center]

    if center == 0:
        assert min(bds) > 0, 'pb input : expected {0} got {1.__class__} : {1}'.format("> 0 ", min(bds))
        res = sorted(-bds) + toadd + list(bds)
    elif center == 1:
        assert min(bds) > 1, 'pb input : expected {0} got {1.__class__} : {1}'.format("> 1 ", min(bds))
        res = sorted(1/bds) + toadd + list(bds)
    else:
        assert 0, f"must be equal to None, 0 or 1, got {center}"
    
    res = numpy.array(res, dtype=float)
    
    return res
    
def create_boundaries(vmin, vmax, symmetric=None, add_center=True, rnd=".2g", **kwarg):
    """
    >>> all(create_boundaries(1,10, symmetric = 0, factor = 3) == numpy.array([-10.,  -7.,  -4.,  -1.,   0.,   1.,   4.,   7.,  10.]))
    True

    """
    res = factor_bounds(vmin, vmax, **kwarg)
    res = norm_numb(res, rnd=rnd)
    if symmetric is not None:
        res = symmetric_bounds(bds=res, center=symmetric, add_center=add_center)
            
    res = norm_numb(res, rnd=rnd)
    res = reduce_list_if_same_value(res)
    res = numpy.array(res, dtype=float)
    
    return res


##======================================================================================================================##
##                                MAIN                                                                                  ##
##======================================================================================================================##


if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        print("Doctests")
        test_result = doctest.testmod()
        print(test_result)
