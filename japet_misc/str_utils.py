"""
Python module for operation on string
"""

##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##

import os
import argparse
import doctest
#~ import datetime
import random
import re

from collections.abc import Iterable
#~ from numbers import Number
from string import ascii_letters as LETTERS


#~ import numpy
import termcolor

#~ from utilss.list_utils import reduce_list

##======================================================================================================================##
##                                FONCTION TEXTES                                                                       ##
##======================================================================================================================##

def try_colors():
    """
    Try term colors and attributes
    """
    print('attributes', end="\n\n")
    for iattr in termcolor.ATTRIBUTES:
        txt = f"attr = {iattr}"
        print(termcolor.colored(txt, attrs=[iattr]))
    
    print('\n\ncolors', end="\n\n")
    for icol in termcolor.COLORS:
        txt = f"color = {icol}"
        print(termcolor.colored(txt, icol))

    return None

def title_text(title, n_title, blank=" " * 2, arrow=None):
    """ Renvoie une fleche ecarte du bord pour un affichage structure
    >>> type(title_text("bon", 1)) == str
    True
    >>> type(title_text("bon", 2))== str
    True
    """
    assert isinstance(title, str), "veuiller entrer un texte"
    assert isinstance(n_title, int), "veuiller entrer un nb entier"

    if arrow is None:
        arrow = ("o", "-", ".", "~")[n_title - 1]
    
    res = "{0}{1} {2}".format(blank * n_title, arrow, title)
    
    if n_title == 1:
        res = res.title() #la methode title renvoie la premiere attre en majuscule
        res = termcolor.colored(res, 'blue', attrs=['bold'])

    return res

def big_title(title, motif="-", n=80):
    """ Renvoie une un titre bien identifier
    >>> type(big_title('tre', '#', 7)) == str
    True
    """
    assert isinstance(title, str), "veuiller entrer un texte"
    res = "{0:{1}<{2}}".format(title.upper(), motif, n)
    res = termcolor.colored(res, 'blue', attrs=['bold', 'reverse'])
    return res

def big_title2(title, motif="-", n=100):
    """ Renvoie une un titre bien identifier
    >>> type(big_title('t', '-', 5)) == str
    True
    """

    assert isinstance(title, str), "veuiller entrer un texte"
    row = "{0:{1}^{2}}".format('', motif, n)
    res = "\n{0}\n{1:^{2}}\n{0}".format(row, title.upper(), n)
    return res

def class_title(obj):
    """
    >>> type(class_title(1)) == str
    True
    >>> type(class_title('rtrd')) == str
    True
    >>> type(class_title([])) == str
    True
    """
    res = ["{0:^50}".format(str(obj.__class__))]
    res += ["{0:^50}\n".format(row_separator(30, "-"))]
    res = '\n'.join(res)
    res = res.upper()
    return res
    
def todo_msg(msg, pyfile):
    """
    >>> type(todo_msg('TODO', "pyfile.py")) == str
    True
    """
    infile = os.path.basename(pyfile).replace(".py", "")
    todomsg = "TODO({0:^15}) --->  {1}".format(infile, msg)
    res = termcolor.colored(todomsg, 'red', attrs=['bold', 'reverse'])
    return res
    
    
def text_import_module(filename):
    """
    >>> 'Module successfully imported  :    module_xyz' in text_import_module("path/module_xyz/__init__.py")
    True
    >>> 'Module successfully imported  :    module_xyz' in text_import_module("path/module_xyz.py")
    True
    """
    if "__init__.py" in filename:
        module = os.path.basename(os.path.dirname(filename))
    else:
        assert ".py" in filename, "{0} is not a python file".format(filename)
        module = os.path.basename(filename.replace(".py", ""))

    res = "Module successfully imported  :    {0}".format(module)
    res = termcolor.colored(res, "magenta")

    return res

def find_sep(string, seps=None):
    """
    Renvoie les separateur trouver dans la chaine de charactere

    PARAM   :    string et separateus
    RETURN  :    list des separateur present dans le string

    >>> find_sep("ad-re_")
    ['-', '_']
    >>> find_sep('ad-re-')
    ['-']
    >>> find_sep('ad')
    []
    >>> find_sep('ad-yu', seps = '_')
    []
    >>> find_sep('ad-yu', seps = '-')
    ['-']
    """
    assert isinstance(string, str), 'Expected {0} got {1.__class__} : {1}'.format("string", string)

    if seps is None:
        seps = ('-', '_', "/", ":", ".")
    elif isinstance(seps, str):
        seps = [seps]
    else:
        assert isinstance(seps, Iterable), 'Expect {0} got {1.__class__} : {1}'.format("listtype", seps)

    res = [sep for sep in seps if sep in string]

    return res

def random_word(size):
    """
    cree un mot alatoire de la taille demande
    >>> type(random_word(5)) == str
    True
    """
    letters = random.sample(LETTERS, size)
    res = "".join(letters)
    return res

def random_text(n_word=100, max_word_size=10):
    """

    >>> type(random_text()) == str
    True
    """
    res = [random_word(random.randint(1, max_word_size)) for iword in range(n_word)]
    res = " ".join(res)
    return res

def match_pattern(pattern, text, replace_stars=True):
    """
    Function doc

    PARAM   :    DESCRIPTION
    RETURN  :    DESCRIPTION

    >>> match_pattern("a", "aaaaaadddd")
    False
    >>> match_pattern("d*", "aaaaaaddddi")
    False
    >>> match_pattern("a*", "aaaaaaddddi")
    True
    >>> match_pattern("a*d", "aaaaaaddddi")
    False
    >>> match_pattern("a*di", "aaaaaaddddi")
    True
    >>> match_pattern("a*a*d*d", "aaaaaaddddi")
    False
    >>> match_pattern("a*a*d*i", "aaaaaaddddi")
    True
    >>> match_pattern('LYON*N*', "LYON BRON")
    True
    >>> match_pattern('LYON*BRON', "LYON BRON")
    True
    """
    assert isinstance(text, str), 'Expected {0} got {1.__class__} : {1}'.format("str", text)

    if replace_stars:
        if pattern[0] != "*":
            pattern = "^" + pattern
        if pattern[-1] != "*":
            pattern = pattern + "$"
        
        
        pattern = pattern.replace("*", r"[\w\W]*") #\w Correspond à n'importe caractère alphanumérique ; équivalent à la classe [a-zA-Z0-9_]. ; \W équivalent à la classe [^a-zA-Z0-9_].
        
    #https://docs.python.org/fr/3/howto/regex.html
    res = re.search(pattern, text)
    res = res is not None
    
    return res
    
def search_pattern(pattern, texts, replace_stars=True):
    """
    
    >>> search_pattern("*pat*", ["pattern", "pte", "apat", "patate"], True)
    ['pattern', 'apat', 'patate']
    >>> search_pattern("pat*", ["pattern", "pte", "apat", "patate"], True)
    ['pattern', 'patate']
    >>> search_pattern("*pat", ["pattern", "pte", "apat", "patate"], True)
    ['apat']
    """
    res = [t for t in texts if match_pattern(pattern=pattern, text=t, replace_stars=replace_stars) is True]
    return res


def row_separator(size, row_sep='-'):
    """
    >>> row_separator(10)
    '----------'
    """
    res = row_sep * size
    return res


def del_doublewhitespace(txtline, nmaxiter=1000):
    """
    Function doc
    >>> del_doublewhitespace('to  do  ')
    'to do '
    >>> del_doublewhitespace('to  do ')
    'to do '
    >>> del_doublewhitespace('to   do ')
    'to do '
    
    """
    res = txtline
    for iiter in range(nmaxiter):
        res = res.replace("  ", " ")
        if "  " not in res:
            break
    else:
        raise ValueError('warning, max iter reached', iiter+1, nmaxiter)
    
    assert "  " not in res, f"pb code: {res}"
    return res
    

def val2str(name, value, fmt, unit):
    """Value to string formating
    >>> val2str("p", 1013.23, ".2f", "pa")
    'p     :    1013.23 (pa)'
    >>> val2str("p", 1013.23, ".0f", "pa")
    'p     :       1013 (pa)'
    """
    res = f"{name:5} : {value:10{fmt}} ({unit})"
    return res

##======================================================================================================================##
##                                PROGRAMME PRINCIPAL                                                                   ##
##======================================================================================================================##

if __name__ == '__main__':

    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        print("Doctests")
        test_result = doctest.testmod()
        print(test_result)
