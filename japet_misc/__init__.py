"""
Module utilss : containing different python utilss
"""


##======================================================================================================================##
##                                IMPORT                                                                                ##
##======================================================================================================================##

#~ import shapely
#~ from shapely import ops, wkt, geometry #evite pb avec shapely  voir a supprimer

import os
import doctest
import argparse
import decimal
import math
import datetime
import string
import warnings
from collections.abc import Iterable
from numbers import Number


import numpy
import matplotlib
from matplotlib import pyplot
import termcolor

# ~ try:
    # ~ matplotlib.use('tkagg')
    # ~ print("used tkagg in matplotlib")
# ~ except:
    # ~ print("cannot use tkagg")



##======================================================================================================================##
##                                IMPORT TO LOAD FUNCTIONS MODULE                                                       ##
##======================================================================================================================##


from japet_misc.numb_utils import *
from japet_misc.list_utils import *
from japet_misc.str_utils import *
from japet_misc.dic_utils import *
#from utilss.color_utils import *
from japet_misc.plot_utils import *
# ~ from japet_misc.import_utils import *
from japet_misc.script_utils import *

##======================================================================================================================##
##                                MISC FUNCTIONS                                                                        ##
##======================================================================================================================##




def newfile_from_pattern(pattern, ext=None):
    """
    Renvoie un nom de fichier nouveau a partir dun pattern et dune extension

    PARAM   :       pattern le chemin initial
                    ext : l'extension
    RETURN  :       nom dun fichier nouveau
    
    >>> import glob, shutil
    >>> path = os.path.dirname(__file__)
    >>> path = os.path.join(path, "test")
    >>> os.mkdir(path)
    
    >>> pattern = "{0}/e".format(path)
    >>> newfile_from_pattern(pattern) == "{0}/e_0".format(path)
    True
    >>> newfile_from_pattern(pattern, "txt") == "{0}/e_0.txt".format(path)
    True
    >>> for i in range(3):f=open(newfile_from_pattern(pattern, "txt"), "w")
    >>> files = glob.glob("{0}*.txt".format(pattern))
    >>> files = sorted(files)
    >>> for f in files: os.path.basename(f)
    'e_0.txt'
    'e_1.txt'
    'e_2.txt'
    >>> shutil.rmtree(path)
    >>> os.path.isdir(path)
    False
    
    """
    directory = os.path.dirname(pattern)

    if ext is None:
        ext = ""
    else:
        ext = f".{ext}"

    assert os.path.isdir(directory), f"pb entree : {directory} attendu {directory}"

    number = 0
    res = ""


    while os.path.isfile(res) or number == 0:
        res = f"{pattern}_{number}{ext}"
        number += 1

    return res


def D(number):
    """
    retourne un nombre decimal a partir d'un string, d'un flottant ou d'un entier
    >>> D(9.16)
    Decimal('9.16')
    >>> D('10.26656')
    Decimal('10.26656')
    >>> D(9.16548989465)
    Decimal('9.16548989465')
    >>> D(11)
    Decimal('11')
    """
    if not isinstance(number, str):
        number = str(number)
    res = decimal.Decimal(number).normalize()
    return res
        

def abs_reldiff(numb1, numb2):
    """

    >>> abs_reldiff(0.5, 1.5)
    1.0
    >>> abs_reldiff(1.5, 0.5)
    1.0
    >>> abs_reldiff(99, 101)
    0.02
    """
    assert isinstance(numb1, Number), f'pb number expected got {numb1.__class__} : {numb1}'
    assert isinstance(numb2, Number), f'pb number expected got {numb2.__class__} : {numb2}'
    mean = numpy.mean([numb1, numb2])
    absdif = abs(numb1 - numb2)
    res = absdif / mean
    return res


def is_bin(x):
    """
    
    >>> is_bin([0,1])
    True
    >>> is_bin(4)
    False
    >>> is_bin([1,2,3])
    False
    >>> is_bin([1, 0])
    False
    """
    res = isinstance(x, Iterable)
    if res:
        res = len(x) == 2
        if res:
            res = x[1] > x[0]
    
    return res




##======================================================================================================================##
##                                CLASSES                                                                               ##
##======================================================================================================================##

class Progress:
    """ Class doc
    >>> p = Progress(10)
    
    p.update()
    p.reset()
    """
    
    def __init__(self, n_iter=numpy.inf):
        """ Class initialiser """
        if isinstance(n_iter, Iterable):
            n_iter = len(n_iter)
        else:
            if n_iter == numpy.inf:
                pass
            else:
                n_iter = int(n_iter)
        
        assert n_iter > 0, f"min val = 1, got : {n_iter}"
        self.n_iter = n_iter
        self.i = 0
        self.str_len = len(str(self.n_iter)) + 1

    def update(self):
        """
        See main doctest
        """
        self.increment()
        
        cnter = self.return_counter()
        res = f'\r In Progress ---> {cnter} <---'

        if self.i >= self.n_iter:
            res += '\n'
                
        print(res, end="")
            
    def increment(self):
        """
        >>> pb = Progress(10)
        >>> pb.i
        0
        >>> pb.increment()
        >>> pb.i
        1
        >>> pb.increment()
        >>> pb.i
        2
        """
        self.i += 1
    
    def return_counter(self):
        """
        
        >>> pb = Progress(10)
        >>> pb.i
        0
        >>> pb.return_counter()
        ' 0  / 10 '
        >>> pb.increment()
        >>> pb.return_counter()
        ' 1  / 10 '
        >>> pb.increment()
        >>> pb.return_counter()
        ' 2  / 10 '
        >>> for i in range(10) : pb.increment()
        >>> pb.return_counter()
        '12  / 10 '
        """
        res = '{0: ^{2}} / {1: ^{2}}'.format(self.i, self.n_iter, self.str_len)
        return res
            #~ return res
            
    def reset(self):
        """
        See main
        """
        self.i = 0
        return None
        
        
        

        
        
##======================================================================================================================##
##                                AVOID TO ADD THESE LINES AT EACH SCRIPTS                                              ##
##======================================================================================================================##


numpy.seterr(all='ignore')
warnings.simplefilter('ignore', DeprecationWarning)

def_plot_params()

pandas.set_option('display.width', 250)

##======================================================================================================================##
##                                MAIN                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':

    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        
        test_result = doctest.testmod()
        print(test_result)
