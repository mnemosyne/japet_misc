"""
Python module for operation on dicts
"""


##======================================================================================================================##
##                                PACKAGE                                                                               ##
##======================================================================================================================##

from collections.abc import Iterable


##======================================================================================================================##
##                CST                                                                                                   ##
##======================================================================================================================##

_keyvalsep = "=="
_element_seps = "[]"

_sep0 = "keyval_sep"
_sep1 = "elmt_seps"
_def_sep_kwarg = {_sep0 : _keyvalsep, _sep1 : _element_seps}

##======================================================================================================================##
##                                FONCTION DICTS                                                                        ##
##======================================================================================================================##

def check_dic_type(dic_type, dic_to_check):
    """
    >>> dt1 = {'meta': 'str', 'mta':str}
    >>> dt2 = {'meta': str, 'mta':float, 'lis':(list, bool)}

    >>> cd1 = {'meta':'false', 'lis':True}
    >>> cd2 = {'meta': 'false', 'mta':'1.01', 'lis':True}
    >>> cd3 = {'meta': 'false', 'mta':1.01, 'lis':[]}

    >>> check_dic_type(dt1, cd1)
    Traceback (most recent call last):
    ...
    AssertionError: dic_type:key meta of value str must be a type got <class 'str'>

    >>> check_dic_type(dt2, cd1)
    Traceback (most recent call last):
    ...
    AssertionError: le dico a tester ne contient pas la cle mta
    >>> check_dic_type(dt2, cd2)
    Traceback (most recent call last):
    ...
    AssertionError: key:value=mta:1.01 is not of type [<class 'float'>] but <class 'str'>
    >>> check_dic_type(dt2, cd3)

    """
    #controle du premier dico : definissant les types.
    for k, v in dic_type.items():
        if isinstance(v, (list, tuple)):
            for i_type in v:
                assert isinstance(i_type, type), 'dic_type:key {0} of value {1} must be a type got {2}'.format(k, v, i_type)
        else:
            assert isinstance(v, type), 'dic_type:key {0} of value {1} must be a type got {2}'.format(k, v, type(v))
            dic_type[k] = [v]

    for k, v in dic_type.items():
        assert k in dic_to_check, 'le dico a tester ne contient pas la cle %s'% k
        test_value = dic_to_check[k]
        type_value = type(test_value)
        assert type_value in v, 'key:value={0}:{1} is not of type {2} but {3}'.format(k, test_value, v, type_value)

    return None
    
def clean_dic(dic):
    """   
    >>> d = {1: {}, 'r': "i"}
    >>> cd = {'r': "i"}
    >>> clean_dic(d) == cd
    True
    >>> e = {i:d for i in range(10)}
    >>> clean_dic(e) == {i:cd for i in range(10)}
    True
    >>> clean_dic({j:{z:z for z in range(j)} for j in range(10)}) == {j:{z:z for z in range(j)} for j in range(1,10)}
    True
    """
    assert isinstance(dic, dict), 'pb input : expected {0} got {1.__class__} : {1}'.format("dict", dic)
    assert dic != {}, "empty dic : {0}".format(dic)
    dic = {k: v for k, v in dic.items() if v != {}}
    res = {}
    for k, v in dic.items():
        if isinstance(v, dict):
            newv = clean_dic(v)
        else:
            newv = v
        if newv != {}:
            res[k] = newv
    #~ res = clean_dic(res)
    return res
    
def dic_key_change_type(dic, new_type=str, strict=True):
    """
    transforme les cle d'un dico en string
    >>> dic_key_change_type({1:1, 2:2}) == {'1': 1, '2': 2}
    True
    >>> dic_key_change_type({'1': 1, '2': 2}, int) == {1: 1, 2: 2}
    True
    >>> dic_key_change_type({'1': 1, 'all': 2}, int)
    Traceback (most recent call last):
            ...
    AssertionError: pb input: connot transfrom all in <class 'int'>
    >>> dic_key_change_type({'1': 1, 'all': 2}, int, False) == {1: 1, 'all': 2}
    True
    """
    res = {}
    for k, v in dic.items():
        try:
            newk = new_type(k)
        except ValueError:
            if strict:
                assert 0, 'pb input: connot transfrom {0} in {1}'.format(k, new_type)
            else:
                newk = k
        res[newk] = v
    return res

def flat_dic(dic, base_key=""):
    """
    >>> d = {i:{j: i for j in range(2)} for i in range(2)}
    >>> d == {0: {0: 0, 1: 0}, 1: {0: 1, 1: 1}}
    True
    >>> d1 =flat_dic(d) 
    >>> d1 == {'00': 0, '11': 1, '01': 0, '10': 1}
    True
    >>> flat_dic(d1) == {'00': 0, '11': 1, '01': 0, '10': 1}
    True
    """
    assert isinstance(dic, dict)
    res = {}
    
    if dic != {}:
        for k, v in dic.items():
            newk = "{0}{1}".format(base_key, k)
            if isinstance(v, dict) and v != {}:
                newval = flat_dic(v, base_key=newk)
                for k1, v1 in newval.items():
                    res[k1] = v1

            else:
                res[newk] = v
    
    return res
    
def inverse_key_value(dic):
    """
    >>> d = {i : i+1 for i in range(10)}
    >>> inverse_key_value(d) == {i+1 : i for i in range(10)}
    True
    >>> d = {i : i+1 for i in range(10)}
    >>> d["e"] = 1
    >>> d["d"] = 2
    >>> d["f"] = 2
    >>> inverse_key_value(d) == {i+1 : i for i in range(10)}
    Traceback (most recent call last):
            ...
    AssertionError: pb input : values are repeated
    1: n=2
    2: n=3
    >>> d = {"a":"b", 1 : "c"}
    >>> inverse_key_value(d) == {"b":"a", "c" : 1}
    True
    """
    assert isinstance(dic, dict), "input {0.__class__}, expected dict".format(dic)
    res = {v: k for k, v in dic.items()}
    
    if len(res) != len(dic): 
        vals = sorted(dic.values())
        rvals = list(set(vals))
        counts = [vals.count(iv) for iv in rvals]
        errmes = ["pb input : values are repeated"]
        errmes += ["{0}: n={1}".format(iv, niv) for iv, niv in zip(rvals, counts) if niv != 1]
        errmes = "\n".join(errmes)
        assert 0, errmes

    return res
    
def infos_to_str(infos, keys=None,  str_fmt=None, **kwargs):
    """
    >>> infos = {"blq" : 1, "i5" : "erete"}
    >>> infos_to_str(infos)
    '[blq==1][i5==erete]'
    >>> infos = {"blq" : 1.025, "i5" : 1.002}
    >>> infos_to_str(infos, str_fmt='.3f')
    '[blq==1.025][i5==1.002]'
    >>> infos_to_str(infos, str_fmt='.2f')
    '[blq==1.02][i5==1.00]'
    >>> infos_to_str(infos, str_fmt='.1f', keyval_sep="::")
    '[blq::1.0][i5::1.0]'
    """
    dic_args = _def_sep_kwarg.copy()
    dic_args.update(kwargs)
    
    keyval_sep = dic_args.pop(_sep0)
    elmt_seps = dic_args.pop(_sep1)
    
    s_par, e_par = elmt_seps
    
    # ~ print(s_par, e_par, keyval_sep)
    
    if keys is None:
        keys = sorted(infos.keys())
            
    res = []
    
    for k in keys:
        v = infos[k]
        if str_fmt is not None:
            # ~ print(k,v)
            v = "{0:{1}}".format(v, str_fmt)
        
        toadd = "{0}{1}{2}{3}{4}".format(s_par, k, keyval_sep, v, e_par)
        res.append(toadd)
            
    res = "".join(res)
    
    return res
    
def str_to_dic_infos(text, **kwargs):
    """
    >>> str_to_dic_infos('[blq==1][i5==erete]') == {"blq" : 1, "i5" : "erete"}
    True
    >>> infos = {"blq" : 1.025, "i5" : 1.002}
    >>> strdic = infos_to_str(infos, str_fmt='.3f')
    >>> strdic
    '[blq==1.025][i5==1.002]'
    >>> for k,v in sorted(str_to_dic_infos(strdic).items()):print(k,v)
    blq 1.025
    i5 1.002
    >>> strdic = infos_to_str(infos, str_fmt='.2f')
    >>> for k,v in sorted(str_to_dic_infos(strdic).items()):print(k,v)
    blq 1.02
    i5 1
    >>> strdic = infos_to_str(infos, str_fmt='.1f')
    >>> for k,v in sorted(str_to_dic_infos(strdic).items()):print(k,v)
    blq 1
    i5 1
    """
    dic_args = _def_sep_kwarg.copy()
    dic_args.update(kwargs)
    
    keyval_sep = dic_args.pop(_sep0)
    elmt_seps = dic_args.pop(_sep1)
    
    s_par, e_par = elmt_seps
    
    texts = text.split(s_par)
    texts = [i.split(e_par)[0] for i in texts]
    keyvals = [i for i in texts if keyval_sep in i]
    
    res = {}
    for keyval in keyvals:
        kv = keyval.split(keyval_sep)
        assert len(kv) == 2, "pb : {0}".format(keyval)
        key = kv[0]
        val = kv[1]
        
        try:
            val = float(val)
            if val.is_integer():
                val = int(val)
        except ValueError:
            pass
                
        res[key] = val
    
    return res
    
def merge_dics(*arg):
    """   
    >>> dics = {"I" : 0, "i" : "r", 1: 5}, {"a": "b", "I" : 'e', "i" : "r", 1: 5}
    >>> merge_dics(dics) == {'I': [0, 'e'], 1: 5, 'i': 'r'}
    True
    >>> dics = {"I" : 0, "i" : "r", 1: 5}, {"a": "b", "I" : 'e', "i" : "r", 1: 5}, {"a": "b", "b" : 'e', "i" : "r", 1: 5}
    >>> merge_dics(dics) == {'i': 'r', 1: 5}
    True
    >>> merge_dics({"I" : 0, "i" : "r", 1: 5}, {"a": "b", "I" : 'e', "i" : "r", 1: 5}, {"a": "b", "b" : 'e', "i" : "r", 1: 5}) == {'i': 'r', 1: 5}
    True
    >>> merge_dics({"I" : 0, "i" : "r", 1: 5}, {"a": "b", "I" : 'e', "i" : "r", 1: 6}, {"a": "b", "b" : 'e', "i":"r", 1: 5}) == {'i': 'r', 1: [5, 6]}
    True

    """
    keys = return_same_keys(*arg)
    
    if len(arg) == 1:
        dics = arg[0]
    else:
        dics = arg
    
    res = {}
    
    for k in keys:
        values = [dic[k] for dic in dics]
        v0 = values[0]
        cond = all([v == v0 for v in values])
        
        if cond:
            res[k] = v0
        else:
            res[k] = list(set(values))
                    
    return res

def return_same_keys(*arg):
    """
    >>> dics = {"I" : 0, "i" : "r", '1': 5}, {"a": "b", "I" : 'e', "i" : "r", '1': 5}
    >>> sorted(return_same_keys(dics))
    ['1', 'I', 'i']
    >>> dics = {"I":0, "i":"r", '1':5}, {"a":"b", "I":'e', "i":"r", '1':5}, {"a":"b", "b":'e', "i":"r", '1':5}
    >>> sorted(return_same_keys(dics))
    ['1', 'i']
    """
    assert len(arg) > 0, "pb entree : {0} attendu {1}".format("arg", arg)
    
    if len(arg) == 1:
        dics = arg[0]
    else:
        dics = arg
    
    assert isinstance(dics, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format("list of dict", dics)

    for i, dic in enumerate(dics):
        keys = dic.keys()
        if i == 0:
            res = set(keys)
        else:
            res = res.intersection(keys)
                
        #~ print(res, keys
                    
    res = list(res)
    return res




##======================================================================================================================##
##                                MAIN                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':

    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    import argparse
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        print("Doctests")
        import doctest
        test_result = doctest.testmod()
        print(test_result)
