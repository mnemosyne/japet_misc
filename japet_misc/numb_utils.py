"""
Python module for operation on number
"""

##======================================================================================================================##
##                                PACKAGE                                                                               ##
##======================================================================================================================##

import math
from collections.abc import Iterable
from numbers import Number

##======================================================================================================================##
##                                FONCTION NUMBS                                                                        ##
##======================================================================================================================##

 


def norm_numb(numb, rnd=".2g"):
    """
    >>> norm_numb(501, rnd = ".3g") == 501
    True
    >>> norm_numb(501, rnd = ".2g") == 500
    True
    >>> norm_numb(551, rnd = ".1g") == 600
    True
    >>> norm_numb(5.01, rnd = ".1g") == 5.0
    True
    >>> norm_numb(5.11, rnd = ".2g") == 5.1
    True
    """
    if isinstance(numb, Number):
        res = f"{numb:{rnd}}"
        res = float(res)
    else:
        assert isinstance(numb, Iterable), f'Expected numb or list got {numb.__class__} : {numb}'
        res = [norm_numb(i, rnd=rnd) for i in numb]
    return res

def normalize_numb(numb, born=None, factor=None):
    """
    Renvoie la puissance de 10 la plus proche

    PARAM   :    nombre et facteur
    RETURN  :    nombre

    >>> normalize_numb(15, "min", 10)
    10
    >>> normalize_numb(15, "max", 10)
    20
    >>> normalize_numb(15, "min", 1e2)
    0
    >>> normalize_numb(15, "max", 1e2)
    100
    >>> normalize_numb(-15, "min", 1e2)
    -100
    >>> normalize_numb(-15, "max", 1e2)
    0
    """
    assert isinstance(numb, Number), f'pb entree demande nombre got {numb.__class__} : {numb}'

    if factor is None:
        if born == "min":
            res = math.floor(numb)
        elif born == "max":
            res = math.ceil(numb)
        else:
            assert 0, f"pb entree : {born} attendu 'min' ou 'max'"
    elif isinstance(factor, str):
        assert born is None
        res = norm_numb(numb, factor)

    else:
        assert isinstance(factor, Number), f'Expected numb got {factor.__class__} : {factor}'
        assert factor > 0, f"pb entree : {factor} attendu > 0"
        newnumb = numb / factor
        res = normalize_numb(numb=newnumb, born=born, factor=None)
        res = res * factor

        if factor >= 1:
            res = int(res)

    return res


def is_odd(numb):
    """
    nombre impair ?

    >>> is_odd(0)
    False
    >>> is_odd(1)
    True
    """
    numb = int(numb)
    
    res = (numb % 2)
    res = bool(res == 1)

    return res

def is_even(numb):
    """
    >>> is_even(0)
    True
    >>> is_even(1)
    False
    """
    res = not is_odd(numb)

    return res


def same_sign(x1, x2):
    """
    >>> math.copysign(1,0)
    1.0
    >>> same_sign(0,1)
    True
    >>> same_sign(-1,1)
    False
    >>> same_sign(-1,-2)
    True
    """
    s1 = int(math.copysign(1, x1))        
    s2 = int(math.copysign(1, x2))        
    res = s1 == s2
    
    return res


##======================================================================================================================##
##                                MAIN                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':

    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    import argparse
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        print("Doctests")
        import doctest
        test_result = doctest.testmod()
        print(test_result)
