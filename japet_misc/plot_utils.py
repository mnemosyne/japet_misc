"""
Python module for ploting, and operation on colors
"""

##======================================================================================================================##
##                                PACKAGE                                                                               ##
##======================================================================================================================##

import argparse
import doctest
import math
import bisect
import time
from collections.abc import Iterable
from numbers import Number
import random

import numpy
import pandas
import matplotlib
from matplotlib import pyplot
from matplotlib.lines import lineStyles, lineMarkers

#~ from utilss.numb_utils import norm_numb
#~ from utilss.list_utils import reduce_list_if_same_value, normalize_list, linvector

##======================================================================================================================##
##                             CST                                                                                      ##
##======================================================================================================================##

FONTS_FAMILIES = ('font.fantasy', 'font.monospace', 'font.sans-serif', 'font.serif')

CMAP_TYPE = (matplotlib.colors.ListedColormap, matplotlib.colors.LinearSegmentedColormap)

RGB_LETTERS = ("r", "g", "b")
COLOR_TYPES = ("hexa", "rgb")

DARK = "dark"
LIGHT = "light"

_cmap_dem = ['#aeefd5', '#b0f2d2', '#b0f2cd', '#b1f2c7', '#b1f3c1', '#b0f4bc', '#b0f5b8', '#b3f6b3', '#b7f7b2', '#bdf7b2', '#c4f7b2', '#c9f8b2',
             '#cffab1', '#d5fab1', '#dcfbb2', '#e3fbb2', '#ebfcb3', '#f2fcb3', '#f9fcb3', '#fbfbb0', '#f4f7a8', '#e8f29d', '#dcee91', '#cee886', '#bfe17b', '#b1db71', '#a2d667', '#93d05d', '#84ca55', '#76c44e', 
             '#66bf46', '#55b93d', '#45b435', '#37ad2e', '#2ba82a', '#22a329', '#1a9d2c', '#15972f', '#109133', '#0b8c36', '#07863a', '#07823e', '#10813f', '#21833e', '#2e863c', '#3a8a3b', '#468d3b', '#51903a',
             '#5d9337', '#699535', '#749833', '#7d9b31', '#849e2f', '#8ea12c', '#98a32a', '#a1a528', '#aba825', '#b5ab23', '#c0af20', '#cab01d', '#d4b21a', '#dfb416', '#e9b411', '#f3b70a', '#f9b505', '#f7ac03', 
             '#f1a002', '#eb9502', '#e58a02', '#df7f02', '#da7402', '#d56a02', '#d16102', '#cb5802', '#c44f02', '#bd4602', '#b73e02', '#b13502', '#ac2d02', '#a62702', '#a02102', '#9a1a02', '#941401', '#901001', 
             '#8a0b01', '#840600', '#7f0400', '#7b0501', '#780b02', '#760f02', '#751103', '#751304', '#751504', '#751504', '#741704', '#731906', '#721c06', '#711e06', '#6f2008', '#6f2208', '#6e2308', '#6d2509', 
             '#6d2709', '#6c280a', '#6c290a', '#6b2b0b', '#6b2c0b', '#6a2d0c', '#6a2f0d', '#6c3210', '#6f3614', '#733c1a', '#75401e', '#774322', '#7b4828', '#7e4c2e', '#815235', '#85583c', '#895d42', '#8b6248', 
             '#8e6750', '#916d57', '#95715d', '#977764', '#9a7e6c', '#9d8474', '#9f8a7d', '#a29086', '#a5978f', '#a69d98', '#a8a4a0', '#abaaa8', '#adadad', '#b0b0b0', '#b3b3b3', '#b7b7b7', '#bababa', '#bebebe', 
             '#c2c2c2', '#c6c6c6', '#cacaca', '#cfcdcf', '#d2d0d2', '#d6d4d6', '#d9d7d9', '#dbd9db', '#dfdddf', '#e3e1e3', '#e7e5e7']
_cmap_score = ['#FFFFFF', '#FFFF00', '#FF0000', "#000000"]
_cmap_rain = ['#FFFF80', '#00FFFF', '#000080', "#FF00FF"]
_cmap_temp = ['#0000FF', "#0080FF", '#DDFFDD', "#FF8000", "#FF0000"]
_cmap_drought = ['#0000FF', '#00FFFF', '#FFFF00', '#800000']
_cmap_par = ["#006000", "#00FF00", "#FFFF00", "#FF0000", "#600000"]
_diff_cmap = ["#000080", "#0080FF", "#FFFFFF", "#FF8000", "#800000"]
_cmap_others = ["#FFFF80", "#FFFF00", "#00FF00", "#008000", "#00FFFF", "#0000FF", "#000080", "#FF00FF", "#FF0000"]
_cmap_greys = ['#DDDDDD', '#222222', "#DDDDDD"]

_classic_cmaps = {"rain" : _cmap_rain, "temp" : _cmap_temp, "drought" : _cmap_drought, "par" : _cmap_par, "diff" : _diff_cmap, "dem" : _cmap_dem,
                  "others" : _cmap_others, "score" : _cmap_score, "grey_alt" : _cmap_greys}


_maxcol = 256


CMAP_FROM_MATPLOT = [i for i in dir(matplotlib.cm) if isinstance(getattr(matplotlib.cm, i), (matplotlib.colors.LinearSegmentedColormap, matplotlib.colors.ListedColormap, dict))]

CMAP_NAMES = list(_classic_cmaps.keys()) + list(CMAP_FROM_MATPLOT)

MARKERS = ["s", "o", "<", ">", "D", "v", "^", "*", "+", "x", "1", "2", "3", "4", "d", "8", "p", "H", "h"]

##======================================================================================================================##
##                COLORS                                                                                                ##
##======================================================================================================================##


def color_cycle(default=True, n=None):
    """
    >>> color_cycle()
    ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
    >>> color_cycle(n=5)
    ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd']
    >>> color_cycle(n=15)
    ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf', '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd']
    """
    if default:
        prop_cycle = pyplot.rcParams['axes.prop_cycle'] 
        res = prop_cycle.by_key()['color'] 
    else:
        raise ValueError("not implemented")
    
    if isinstance(n, Iterable):
        n = len(n)
    
    if isinstance(n, int):
        res = [res[i%len(res)] for i in range(n)]
        
        
    
    return res
    

def is_hexa(hexa):
    """
    >>> is_hexa('#000000')
    True
    >>> is_hexa('#00000')
    Traceback (most recent call last):
    ...
    AssertionError: hexa doit contenir 6 chiffre got: 00000 len = 5
    >>> is_hexa('000000')
    Traceback (most recent call last):
    ...
    AssertionError: invalid first value : expected "#" got: "0" from 000000
    >>> is_hexa('#00000h')
    Traceback (most recent call last):
    ...
    ValueError: invalid literal for int() with base 16: 'h'
    """
    assert hexa[0] == '#', 'invalid first value : expected "#" got: "{0}" from {1}'.format(hexa[0], hexa)
    hexa = hexa.lstrip('#')
    assert len(hexa) == 6, "hexa doit contenir 6 chiffre got: {0} len = {1}".format(hexa, len(hexa))
    for i in hexa:
        int(i, 16) # si erreur alors la lettre est superieur a f

    res = True
    return res

def hex_to_rgb(hexa):
    """
    >>> hex_to_rgb("#000000") == {'r': 0, 'b': 0, 'g': 0}
    True
    >>> hex_to_rgb("#ffffff") == {'r': 255, 'b': 255, 'g': 255}
    True
    >>> hex_to_rgb("#ff0000")['r']
    255
    >>> hex_to_rgb("#00ff00")['g']
    255
    >>> hex_to_rgb("#0000ff")['b']
    255
    """
    assert is_hexa(hexa), ''

    hexa = hexa.lstrip('#')
    res = {}

    for i, color in enumerate(['r', 'g', 'b']):
        i_start = i * 2
        i_end = (i+1) * 2
        h_value = hexa[i_start:i_end]     #couleur 'col' en hexa
        c_value = int(h_value, 16)            #couleur en entier base 16 : 00 --> 0 ff --> 255
        res[color] = c_value
        
    return res

def rgb_to_hex(rgb):
    """
    >>> rgb_to_hex({'r': 0, 'b': 0, 'g': 0})
    '#000000'
    >>> rgb_to_hex({'r': 255, 'b': 255, 'g': 255})
    '#ffffff'
    >>> rgb_to_hex({'r': 255, 'b': 0, 'g': 0})
    '#ff0000'
    >>> rgb_to_hex({'r': 0, 'b': 0, 'g': 255})
    '#00ff00'
    >>> rgb_to_hex({'r': 0, 'b': 255, 'g': 0})
    '#0000ff'
    >>> rgb_to_hex((0, 0, 255))
    '#0000ff'
    """
    
    if isinstance(rgb, dict):
        r = rgb['r']
        g = rgb['g']
        b = rgb['b']
    
    elif isinstance(rgb, Iterable):
        if len(rgb) == 3:
            pass
        elif len(rgb) == 4:
            assert rgb[3] == _maxcol - 1, "alpha paramter is expected to be 1, got {0}".format(rgb)
        else:
            raise ValueError("3 colors are expected, got {0}".format(rgb))

        r = rgb[0]
        g = rgb[1]
        b = rgb[2]
            
    else:
        raise ValueError("Not implementd")
        
    r = int(round(r))
    b = int(round(b))
    g = int(round(g))
    res = "#{0:02x}{1:02x}{2:02x}".format(r, g, b)
    assert len(res) == 7, "expected hexa, got {0}".format(res)

    return res
    
def std_rgb_col(rgb):
    """
    >>> std_rgb_col(0)
    0
    >>> std_rgb_col(-10)
    0
    >>> std_rgb_col(128)
    128
    >>> std_rgb_col(255)
    255
    >>> std_rgb_col(256)
    255
    """
    assert isinstance(rgb, Number), 'pb input : expected {0} got {1.__class__} : {1}'.format('numb', rgb)
    if rgb < 0:
        res = 0
    elif rgb > 255:
        res = 255
    else:
        res = rgb

    return res
    
def std_rgb_dic(rgb):
    """
    >>> std_rgb_dic({'r':200, 'b' : 10, 'g': 255}) == {'r':200, 'b' : 10, 'g': 255}
    True
    >>> std_rgb_dic({'r':200, 'b' : -10, 'g': 257}) == {'r':200, 'b' : 0, 'g': 255}
    True
    """
    assert isinstance(rgb, dict), 'pb input : expected {0} got {1.__class__} : {1}'.format('dict', rgb)
    res = {k : std_rgb_col(v) for k, v in rgb.items()}
    return res
    
def define_color_type(color):
    """
    >>> define_color_type('#00ff00')
    'hexa'
    >>> define_color_type({'r':200, 'b' : 0, 'g': 255})
    'rgb'
    """
    if isinstance(color, str):
        if is_hexa(color):
            res = "hexa"
        else:
            assert 0, "pb input {0}".format(color)
    elif isinstance(color, dict):
        for k, v in color.items():
            assert k in RGB_LETTERS, "pb rgb : {0}".format(k)
            assert 0 <= v <= 255, "value pb : {0}".format(v)
        
        res = "rgb"
    
    else:
        assert 0, "pb input {0}".format(color)

    
    assert res in COLOR_TYPES, "pb code"
    return res

def rgbtupple_todic(rgb, mul_255=False):
    """
    >>> rgbtupple_todic((0,0.5,1), True) == {'g': 128, 'b': 255, 'r': 0}
    True
    >>> rgbtupple_todic((0,128,255), False) == {'g': 128, 'b': 255, 'r': 0}
    True
    """
    assert isinstance(rgb, tuple)
    assert len(rgb) == 3
    
    res = {k : v for k, v in zip(RGB_LETTERS, rgb)}
    
    if mul_255:
        assert all(0 <= v <= 1 for v in res.values()), "pb : {0}".format(res)
        res = {k: round(v *255) for k, v in res.items()}
    
    res = {k : int(v) for k, v in res.items()}
    
    res = std_rgb_dic(res)
    return res

class Color:
    """ Class doc 
    >>> c1 = Color("#001000")
    >>> c2 = Color("#0000AA")
    
    >>> c3 = c1 + c2
    >>> c3.hexa
    '#0010aa'
    >>> c4 = c3 - c1
    >>> c4.hexa
    '#0000aa'
    >>> fig = pyplot.figure()
    >>> l = c1.plot()
    >>> fig.show()
    >>> pyplot.close()
    >>> c1.kind()
    'dark'
    >>> c1.is_darkcolor()
    True
    >>> Color("#888888").kind() is None
    True
    >>> Color("#FFEEEE").kind()
    'light'
    >>> Color("#FFEEEE").is_lightcolor()
    True
    """

    def __init__(self, color):
        """ Class initialiser """
        
        color_type = define_color_type(color)
        
        if color_type == "rgb":
            self.rgb = color
            self.hexa = rgb_to_hex(color)
        else:
            self.hexa = color
            self.rgb = hex_to_rgb(color)

    def __getitem__(self, k):
        """
        Get color value
        """
        if k in self.__dict__:
            res = self.__dict__[k]
        else:
            assert k in RGB_LETTERS
            res = self.rgb[k]

        return res
            
            
    def __add__(self, other):
        """
        ADD two color
        """
        assert isinstance(other, (Color, dict)), 'pb input : expected {0} got {1.__class__} : {1}'.format('Color', other)
        
        res = {k : self[k] + other[k] for k in RGB_LETTERS}
        res = std_rgb_dic(res)
        res = Color(res)
                
        return res
            
    def __sub__(self, other):
        """
        SUB two color
        """
        assert isinstance(other, (Color, dict)), 'pb input : expected {0} got {1.__class__} : {1}'.format('Color', other)
        
        res = {k : self[k] - other[k] for k in RGB_LETTERS}
        res = std_rgb_dic(res)
        res = Color(res)
                
        return res

    def plot(self):
        """
        Random plot of the color
        """
        n = 5
        x = sorted([random.random() for i in range(n)])
        y = sorted([random.random() for i in range(n)])
        res = pyplot.plot(x, y, marker='s', ls='-', color=self.hexa)
        res = res[0]
        
        return res
    
    def kind(self, lim=32):
        """Return light, dark or None"""
        vals = list(self.rgb.values())
        vals = numpy.array(vals, dtype=float)
        mxlm = 256 - lim
        if any(vals < lim):
            res = DARK
        elif all(vals > mxlm):
            res = LIGHT
        else:
            res = None
        return res
    
    def is_darkcolor(self, lim=32):
        """True if is dark"""
        res = self.kind(lim=lim) == DARK
        return res
    
    def is_lightcolor(self, lim=32):
        """True if is light"""
        res = self.kind(lim=lim) == LIGHT
        return res
        


def mix_colors(*colors):
    """
    >>> mix_colors(["#000000", "#FFFFFF"])
    '#808080'
    """
    if len(colors) == 1:
        colors = colors[0]
    
    reds = []
    greens = []
    blues = []       
    
    for col in colors:
        rgb = hex_to_rgb(col)
        r = rgb["r"]
        g = rgb["g"]
        b = rgb["b"]
        
        reds.append(r)
        greens.append(g)
        blues.append(b)

    dic = {k : numpy.mean(vals) for k, vals in zip(["r", "g", "b"], [reds, greens, blues])}
    res = rgb_to_hex(dic)
    return res


def primary_colors():
    """
    Renvoie une list de couleurs compose d'une ou deux couleur primaire
    >>> primary_colors()
    ['#ff0000', '#0000ff', '#ff00ff', '#00ff00', '#ffff00', '#00ffff']
    """
    vals = (0, 255)
    res = []

    for g in vals:
        for b in vals:
            for r in vals:
                if not r == g == b:
                    res.append({'r' : r, 'b': b, 'g' : g})

    res = [rgb_to_hex(i) for i in res]
    return res


def max_contrast_for_n_colors(n_colors, add_color=None):
    """
    Return the max contrast betwenn n color
    
    >>> max_contrast_for_n_colors(6)
    ['#ff0000', '#0000ff', '#ff00ff', '#00ff00', '#ffff00', '#00ffff']
    >>> max_contrast_for_n_colors(7)
    ['#ff0000', '#0000ff', '#ff00ff', '#00ff00', '#ffff00', '#00ffff', '#aa5555']
    >>> max_contrast_for_n_colors(8)
    ['#ff0000', '#0000ff', '#ff00ff', '#00ff00', '#ffff00', '#00ffff', '#aa5555', '#5555aa']
    """
    pcols = primary_colors()
    npc = float(len(pcols))
    
    if add_color is None:
        res = []
    elif isinstance(add_color, str):
        res = [add_color]
    elif isinstance(add_color, Iterable):
        res = list(add_color)
    else:
        assert 0, "not implemented"
    
    if len(pcols) > n_colors:
        res = pcols
    else:
            
        n_turn = math.ceil(n_colors / npc)
        n_turn = int(n_turn)
        #~ print n_turn
        
        for nblack in range(n_turn):
            npcol = n_turn - nblack
            nwhite = nblack
            for pcol in pcols:
                col1 = ["#000000"] * nblack
                col2 = ["#FFFFFF"] * nwhite
                col3 = [pcol] * npcol
                colors = col1 + col2 + col3
                #~ print colors
                newcol = mix_colors(colors)

                res.append(newcol)

    res = res[:n_colors]
    
    assert all(is_hexa(i) for i in res), f'pb input, result= {res.__class__} : {res}'
    
    return res

    
def dark_colors(lim=32):
    """
    >>> len(dark_colors())
    41
    """
    res = [icol for icol, ihexa in matplotlib.colors.cnames.items() if Color(ihexa).is_darkcolor(lim=lim)]
    return res
    
def rdm_color(seed=None, dark_color=True):
    """
    >>> c = Color(matplotlib.colors.cnames[rdm_color(seed = 10)])
    >>> c.is_darkcolor()
    True
    """
    random.seed(seed)
    if not dark_color:
        cols = list(matplotlib.colors.cnames.keys())
    else:
        cols = dark_colors()

    res = random.choice(cols)
    #~ print(res)
    return res
    
def rdm_marker(seed=None):
    """
    Function doc
    >>> rdm_marker() in MARKERS
    True
    """
    random.seed(seed)
    res = random.choice(MARKERS)
    #~ print(res)
    return res

    
##======================================================================================================================##
##                             FIGURES                                                                                  ##
##======================================================================================================================##

def close_all(slowly=0):
    """
    Close all figures
    >>> close_all()
    Deprecated, use directly pyplot.close('all')
    >>> pyplot.get_fignums()
    []
    """
    print("Deprecated, use directly pyplot.close('all')")
    while pyplot.get_fignums():
        if slowly:
            time.sleep(slowly)
        pyplot.close()
    return None

def show_all(slowly=0):
    """ Show all figures """
    for i_fig in pyplot.get_fignums():
        fig = pyplot.figure(i_fig)
        fig.show()
        if slowly:
            time.sleep(slowly)

def choice_show_or_close(show=True):
    """
    Choix entre motrer les figure ou non si oui choix entre ereur ou non danner a lutilisateur
    """
    if show:
        show_all()
        text = input('tapez entrer pour continuer et fermer les figure tute autres touches pour erreur')
    else:
        text = ''
    pyplot.close('all')
    assert text == '', "chose due : chose faite"

    return None


def show_and_close(slowly=0.1):
    """Show and close slowly"""
    show_all(slowly=slowly)
    pyplot.close('all')

##======================================================================================================================##
##                                COLORS   AND STYLES                                                                   ##
##======================================================================================================================##

def plot_colors(names='all'):
    """plot"""
    if names == 'all':
        names = sorted(matplotlib.colors.cnames.keys())

    if isinstance(names, str):
        names = [names]

    #~ pbar = ProgressBar(len(names))
    n = 10
    x = numpy.random.normal(size=10)
    y = numpy.linspace(0.1, 0.9, n, endpoint=True)
    
    cond = True
    icolor = 0
    imax = 1.5 * len(names)
    res = []

    while icolor < imax and cond:
        fig, axs = pyplot.subplots(nrows=5, ncols=7, figsize=(20, 14))
        axs = axs.reshape(axs.size)
        for ax in axs:
            cond = icolor < len(names)
            if cond:
                #~ print(icolor, len(names), names[icolor])
                cname = names[icolor]
                assert cname in matplotlib.colors.cnames or is_hexa(cname), 'couleur non dispo'
                ax.set_title(cname)
                ax.plot(x, y, 'o', mfc=cname, mec='k', ms=20)
                ax.grid(1)
                ax.set_xticklabels(["" for i in ax.get_xticks()])
                ax.set_yticklabels(["" for i in ax.get_yticks()])
                icolor += 1
            else:
                break
        fig.show()
        res.append(fig)
        #~ assert 0
    
    assert cname == names[-1]
    #~ time.sleep(1)
    #~ pyplot.close('all')
    
    return res
    
def plot_primary_colors():
    """
    Fait un graphqie pour voir les couleur primaire
    >>> plot_primary_colors()
    >>> show_all(0.1)
    >>> pyplot.close('all')
    """

    res = primary_colors()
    pyplot.figure(figsize=(14, 8))

    for i, color in enumerate(res):
        y = [i] * 10
        x = range(10)
        pyplot.plot(x, y, 'o', mfc=color, ms=20, label=color)

    pyplot.xlim(-1, 11)
    pyplot.ylim(-1, 7)
    pyplot.grid(1)
    pyplot.legend()

    show_all()

def plot_primary_color_list(n=20):
    """
    Montre les differentes couleur obtenue en mixant les couleur primaires
    >>> plot_primary_color_list()
    >>> show_all(0.1)
    >>> pyplot.close('all')
    """
    col_list = primary_colors()

    res = []

    for i1, c1 in enumerate(col_list):
        for i2 in range(i1 + 1, len(col_list)):
            c2 = col_list[i2]
            res.append([c1, c2])

    col_list = [linear_list_color(*i, n=n) for i in res]

    pyplot.figure(figsize=(14, 8))

    for y, lcols in enumerate(col_list):
        for x, col in enumerate(lcols):
            pyplot.plot(x, y, 'o', mfc=col, ms=10)
    show_all()

def plot_color_list(clist=None, n_markers=5):
    """
    >>> plot_color_list()
    >>> show_all(0.1)
    >>> pyplot.close('all')
    """
    if clist is None:
        clist = ['#ff0000', "#ffff00", "#00ffff", "#ff00ff", "#0000ff", "#00ff00"]

    fig = pyplot.figure(figsize=(14, 8))
    ax = fig.gca()


    for x, icolor in enumerate(clist):
        xs = [x] * n_markers
        ys = range(n_markers)

        pyplot.plot(xs, ys, 'o', mec=icolor, mfc="w", mew=2, ms=10)
    
    ax.set(xticklabels=clist, xticks=numpy.arange(len(clist)))

    show_all()

def plot_style(style_list='all', mec='k', mfc='r'):
    """
    Grpah les different marker disponibles
    >>> plot_style()
    >>> show_all(0.1)
    >>> pyplot.close('all')
    """
    if style_list == 'all':
        plot_style(style_list="lines", mec=mec, mfc=mfc)
        plot_style(style_list="markers", mec=mec, mfc=mfc)
    else:
        fig = pyplot.figure(figsize=(14, 8))
        ax = fig.gca()

        if style_list == "markers":
            styles = lineMarkers
        elif style_list == "lines":
            styles = lineStyles
        else:
            raise ValueError("not implemented")
            
        styles = list(styles.keys())
            
        x_s = []
        y_s = []
        for i, style in enumerate(styles):

            if style_list == "markers":
                x = i % 5
                y = i / 5
                xs = [x + d for d in (-0.2, -0.2, 0.2, 0.2)]
                ys = [y + d for d in (-0.2, 0.2, 0.2, -0.2)]
                ls = 'none'
                marker = style
                line = pyplot.plot(xs, ys, ls=ls, marker=marker, label=style)[0]
                line.set_mfc(mfc)
                line.set_mec(mec)
                line.set_ms(12)
                pyplot.text(x, y, style)
            elif style_list == "lines":
                xs = [0, 10]
                ys = [i] * 2
                ls = style
                marker = 'none'
                line = pyplot.plot(xs, ys, ls=ls, label=style)[0]
                x = numpy.mean(xs)
                y = numpy.mean(ys)
                pyplot.text(x, y, style)
            else:
                raise ValueError("{0} not available".format(style))
            
            x_s.append(xs)
            y_s.append(ys)
            
        #~ print(x_s)
        x_s = numpy.concatenate(x_s)
        y_s = numpy.concatenate(y_s).flatten()
        #~ print(x_s)
        #~ print(y_s)
        d = 1
        xlim = (min(x_s) - d, max(x_s) + d)
        ylim = (min(y_s) - d, max(y_s) + d)
        #~ print(xlim, ylim)
        ax.set(xlim=xlim, ylim=ylim)
        #~ ax.set_xlim(ax.get_xlim()[0]-1, ax.get_xlim()[1]+1)
        #~ ax.set_ylim(ax.get_ylim()[0]-1, ax.get_ylim()[1]+1)

        pyplot.grid(1)
        #~ pyplot.legend(numpoints = 1)
        show_all()



def rdm_linearg(seed=None):
    """
    Random line argument in a dic
    >>> f = pyplot.figure()
    >>> dp = rdm_linearg()
    >>> l = pyplot.plot([0, 1], [0, 1], **dp)
    >>> show_and_close()
    """
    random.seed(seed)
    col = rdm_color(seed=seed)
    ls = random.choice(["-", '--', ":"])
    res = {"color" : col, "ls" : ls}
    return res



##======================================================================================================================##
##                COLORMAP                                                                                              ##
##======================================================================================================================##


def get_colormap(name, ncol=256, extend=True, fmt=None):
    """
    >>> j=get_colormap('jet_r')
    >>> v=get_colormap('viridis_r')
    >>> r=get_colormap('rain')
    >>> j.N
    256
    >>> v.N
    256
    >>> r.N
    256
    >>> get_colormap('jet', extend = False,ncol = 5, fmt = list)
    ['#000080', '#0080ff', '#7dff7a', '#ff9800', '#800000']
    >>> get_colormap('viridis', extend = False, ncol = 5, fmt = list)
    ['#440154', '#3b528b', '#21918c', '#5cc863', '#fde725']
    """
    if name in CMAP_FROM_MATPLOT:
        lscm = matplotlib.cm.get_cmap(name)
    else:
        if "rev" in name:
            name = name.replace("rev", "")
            rev = True
        else:
            rev = False

        cols = _classic_cmaps[name]
        if rev:
            cols = list(reversed(cols))
        
        lscm = matplotlib.colors.LinearSegmentedColormap.from_list(name, colors=cols)
    
    if extend is True:
        ncol = ncol + 2
    else:
        assert extend is False, "extend must be True or False"
    
    inds = numpy.round(numpy.linspace(0, _maxcol - 1, ncol))
    inds = numpy.array(inds, dtype=int)
    #~ print(inds)
    
    colors = lscm(inds)
    #~ print(colors)
    if extend:
        under = colors[0]
        over = colors[-1]
        colors = colors[1:-1]
    
    res = matplotlib.colors.ListedColormap(colors=colors)
    
    if extend:
        res.set_under(under)
        res.set_over(over)
        assert (res(-1) == under).all() and (res(res.N) == over).all()
        assert (res(0) != under).any() and (res(res.N -1) != over).any()
    
    if fmt is None:
        pass
    elif fmt is list:
        #~ print(res.colors)
        res = [rgb_to_hex(i * (_maxcol - 1)) for i in res.colors]
        
    else:
        raise ValueError("fmt {0} not implemented".format(fmt))
    
    return res

def linear_list_color(*colors, n=10):
    """
    >>> linear_list_color("#FFFFFF" , "#000000", n= 3)
    ['#ffffff', '#808080', '#000000']
    >>> linear_list_color(['#ff0000', "#ffff00","#00ffff","#ff00ff","#0000ff","#00ff00"], n = 11)
    ['#ff0000', '#ff8000', '#ffff00', '#7fff80', '#00ffff', '#7f80ff', '#ff00ff', '#7f00ff', '#0000ff', '#008080', '#00ff00']
    """
    if len(colors) == 1:
        colors = colors[0]
        assert isinstance(colors, (list, tuple, numpy.ndarray))
        
    
    cm = matplotlib.colors.LinearSegmentedColormap.from_list('a', colors=colors, N=n)
    cs = cm(numpy.arange(n))
    cs = cs[:, :3] * (_maxcol - 1)
    #~ print(cs)
    res = [rgb_to_hex(irow) for irow in cs]

    return res





##======================================================================================================================##
##                                COLORMAP                                                                              ##
##======================================================================================================================##
def plot_colorbar(*boundaries, cmap="viridis", extend=True, **kwarg):
    """
    >>> cb = plot_colorbar(numpy.arange(10), extend = True)
    >>> cb.extend
    'both'
    >>> cb = plot_colorbar(numpy.arange(10), extend = False)
    >>> cb.extend
    'neither'
    >>> show_and_close()
    """
    #treat args and kwargs
    if not boundaries:
        print("warmiong: no bounds are given")
        boundaries = numpy.arange(0, 1.01, 0.1)
    elif len(boundaries) == 1:
        boundaries = boundaries[0]
    else:
        pass
        
    bds = numpy.array(boundaries)
    
    
    if "cax" in kwarg:
        axcb = kwarg.pop("cax")
    elif "ax" in kwarg:
        axcb = kwarg.pop("ax")
    else:
        if "pos" in kwarg:
            pos = kwarg.pop("pos")
        else:
            #~ pos = [0.1,0.1,0.8,0.8]
            pos = [0.9, 0.1, 0.05, 0.8]
            
        axcb = pyplot.axes(pos)

 
    argdic = {"orientation" : 'vertical'}
    argdic.update(kwarg)

    #~ fig = pyplot.gcf()

    nbds = len(bds)
    #~ print("bds",nbds, ":", bds)
    ncol = nbds - 1
    cm = get_colormap(name=cmap, ncol=ncol, extend=extend)
    #~ print("cmap", cm.N)
     
    nrm = matplotlib.colors.BoundaryNorm(bds, ncol)
    
    if extend  is True:
        extd = "both"
    elif extend is False:
        extd = "neither"
    else:
        raise ValueError("extend is required, True or False, got {0}".format(extend))
    
    res = matplotlib.colorbar.ColorbarBase(axcb, cmap=cm, norm=nrm, extend=extd, **argdic)
    #~ axcb.grid(color='k', linestyle='-', linewidth=1)
    #~ res.set_label(cmap)
    
    return res

def plot_cmap(names='all_nr', ncb_p_f=10, nbds=10, **kwarg):
    """
    >>> plot_cmap('jet')
    >>> show_all(0.1)
    >>> pyplot.close('all')
    """

    #~ cmnames = list(CMAP_NAMES) + list(CMAPS_CLASSICS)

    if 'all' in names:
        if "nr" in names:
            names = [i for i in CMAP_NAMES if i[-2:] != "_r"]
        elif "_r" in names:
            names = [i for i in CMAP_NAMES if i[-2:] == "_r"]
        else:
            names = CMAP_NAMES
            
    if isinstance(names, str):
        names = [names]
    
    if isinstance(names, Iterable):
        names = sorted(names)
            
    xs = numpy.linspace(0.05, 0.95, ncb_p_f)
    dx = numpy.diff(xs).mean()

    for icb, icmap in enumerate(names):
        assert icmap in CMAP_NAMES, '{0} pas disponible dans {1}'.format(icmap, CMAP_NAMES)
        index = icb % ncb_p_f
        
        if index == 0:
            pyplot.figure(figsize=(14, 16))
                
        
        x = xs[index]
        
        cbpos = (0.1, x, 0.8, dx/4)
        
        bds = numpy.arange(nbds)
        ori = "horizontal"
        cb = plot_colorbar(bds, cmap=icmap, pos=cbpos, orientation=ori, **kwarg)
        #~ cb = plot["cbar"]
        #~ axcb = plot["ax_cb"]
        #~ axcb.set_xlabel(icmap)
        #~ axcb.set_xticklabels(["" for i in range(11)])
        cb.set_label(icmap)
        cb.ax.grid(color='k', linestyle='-', linewidth=1)

    
        show_all()
                
##======================================================================================================================##
##                                PLOT  FNCTIONS                                                                        ##
##======================================================================================================================##

def plot_pie_diagramm(x, y, radius, fracs, colors, names=None):
    """
    Realise un diagramme camenbert aux positions fournies avec les fractions passees entree

    PARAM   :       - x et y : nombre definissant la position du camenbert
                    - radius : taille du camendbert
                    - fracs liste de fraction
                    - colors : liste de couleurs
    RETURN  :       Plot un camenbert

    >>> piediag = plot_pie_diagramm(0, 0, 1, [0.1, 0.3, 0.25, 0.35], ['b', 'k', 'r', 'c'])
    """
    assert isinstance(fracs, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format('list', fracs)
    assert abs(sum(fracs) - 1) < 1e-8, 'pb entree la somme ne fait pas 1 got {0.__class__} : {0}'.format(sum(fracs))
    assert isinstance(x, Number), 'pb entree demande {0} got {1.__class__} : {1}'.format("nombre", x)
    assert isinstance(y, Number), 'pb entree demande {0} got {1.__class__} : {1}'.format("nombre", y)
    assert isinstance(radius, Number), 'pb entree demande {0} got {1.__class__} : {1}'.format("nombre", radius)
    assert isinstance(colors, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format('list', colors)
    assert len(colors) == len(fracs), "pb entree : {0} attendu {1}".format(len(colors), len(fracs))
    if names is None:
        names = colors

    ax = pyplot.gcf().gca()
    center = (x, y)

    fracs = numpy.array(fracs, dtype=float)
    thetas = 360 * fracs
    theta0 = 0
    wedges = []
    #~ print thetas
    for theta, color in zip(thetas, colors):
        theta1 = theta0 + theta
        #~ print theta0, theta1

        wedge = matplotlib.patches.Wedge(center, radius, theta0, theta1, edgecolor="k", facecolor=color)
        theta0 += theta

        ax.add_artist(wedge)
        wedges.append(wedge)

    pyplot.legend(wedges, names)



    return wedge

def plot_histogram(x, y, width, fracs, colors, total_height=None, names=None):
    """
    Realise un diagramme camenbert aux positions fournies avec les fractions passees entree

    PARAM   :       - x et y        :       position de l'histogramme : nombre
                    - width         :       largeur : nombre
                    - fracs         :       fraction : liste de nombre dont la somme fait 1
                    - colors        :       couleurs

    RETURN  :    graph un histogramm

    >>> hist = plot_histogram(0, 0, 1, [0.2, 0.3, 0.1, 0.4], ['r', 'k', 'c', "b"])
    """
    assert isinstance(fracs, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format('list', fracs)
    assert abs(sum(fracs)) - 1 < 1e-8, 'pb entree la somme ne fait pas 1 got {0.__class__} : {0}'.format(sum(fracs))
    assert isinstance(x, Number), 'pb entree demande {0} got {1.__class__} : {1}'.format("nombre", x)
    assert isinstance(y, Number), 'pb entree demande {0} got {1.__class__} : {1}'.format("nombre", y)
    assert isinstance(width, Number), 'pb entree demande {0} got {1.__class__} : {1}'.format("nombre", width)
    assert isinstance(colors, Iterable), 'pb entree demande {0} got {1.__class__} : {1}'.format('list', colors)
    assert len(colors) == len(fracs), "pb entree : {0} attendu {1}".format(len(colors), len(fracs))

    if names is None:
        names = ["" for i in range(len(fracs))]
    else:
        assert len(names) == len(fracs), "pb entree : {0} attendu {1}".format(len(names), len(fracs))

    if total_height is None:
        total_height = 1
    else:
        assert isinstance(total_height, Number), 'Input: numb exp got {0.__class__} : {0}'.format(total_height)

    res = []

    t_width = len(fracs) * width
    x0 = x - t_width / 2.0
    x1 = x + t_width / 2.0
    x = x0
    ori = 'vertical'
    ali = 'edge'
    for frac, col, nm in zip(fracs, colors, names):
        ht = frac * total_height
        b = pyplot.bar(x=x, height=ht, width=width, bottom=y, color=col, edgecolor="k", align=ali, orientation=ori, label=nm)
        x += width
        res.append(b)

    pyplot.plot([x0, x1], [y, y], color="k")

    return res

def plot_text(text, **kwarg):
    """
    >>> locs = ["{0} - {1}".format(i,j) for i in ("upper", "middle", "lower") for j in ("left", "center", "right")]
    >>> f = pyplot.figure()
    >>> for l in locs : p=plot_text(l, loc = l)
    >>> show_all(0.1)
    >>> pyplot.close('all')
    """
    def_x = 0.05
    def_y = 0.95
    
    if "loc" in kwarg:
        loc = kwarg["loc"]
        del kwarg["loc"]
    else:
        loc = "lower center"
            
            
    if "upper" in loc:
        def_y = 0.95
        valign = "top"
    elif "lower" in loc:
        def_y = 0.05
        valign = "bottom"
    elif "middle" in loc:
        def_y = 0.5
        valign = "center"
    else:
        assert 0, "if loc is given it must contain x and y infos : {0}".format(loc)
    
    if "right" in loc:
        def_x = 0.95
        halign = 'right'
    elif "left" in loc:
        def_x = 0.05
        halign = 'left'
    elif "center" in loc:
        def_x = 0.5
        halign = 'center'
    else:
        assert 0, "if loc is given it must contain x and y infos : {0}".format(loc)
    
    bbox = {'facecolor' : 'w', 'alpha' : 1}
    
    if 'facecolor' in kwarg:
        bbox["facecolor"] = kwarg["facecolor"]
        del kwarg["facecolor"]
    
    if 'alpha' in kwarg:
        bbox["alpha"] = kwarg["alpha"]
        del kwarg["alpha"]
                    
    argdic = {'x' : def_x, 
              'y' : def_y,
              'horizontalalignment'   :       halign, 
              'verticalalignment'     :       valign,
              'transform'             :       pyplot.gca().transAxes,
              'bbox'                  :       bbox,
              'name'                  :       'monospace', 
              'fontsize'              :       14}
    
    argdic.update(kwarg)
    
    
    res = pyplot.text(s=text, **argdic)

    return res
        
def plot_fonts(family):
    """
    Plot font examples
    """
    pyplot.figure()
    pyplot.title(family)
    
    fm = matplotlib.font_manager.FontManager()
            
    assert family in FONTS_FAMILIES
    fonts = matplotlib.rcParamsDefault[family]
    ys = numpy.linspace(0.05, 0.95, len(fonts))
    for k_f, y in zip(fonts, ys):
        try:
            ttfname = fm.findfont(k_f)
            ttfname = ttfname.split("/")[-1]
            text = "{0} : {1}".format(k_f, ttfname)
            #~ print(text)
            pyplot.text(s=text, y=y, x=0.05, family=k_f)
            show_all()
        except ValueError:
            print(k_f)

def plot_multimarkers(x, y, markers=("1", "2", "3", "4"), colors=('r', 'b', 'g', "k"), **kwargs):
    """
    >>> l = plot_multimarkers(numpy.arange(10), numpy.arange(10))
    >>> show_all()
    """
    # options
    argdic = dict(ls="", mfc="w", ms=6, mew=1.5)
    argdic.update(kwargs)
    
    #values and markers
    nx = len(x)
    assert nx == len(y)
    nmk = len(markers)
    mks = [markers[i%nmk] for i in range(nx)]
    ncol = len(colors)
    mecs = [colors[i%ncol] for i in range(nx)]
    assert len(mecs) == len(mks) == nx
    
    res = [pyplot.plot(ix, iy, marker=imk, mec=imec, **argdic) for ix, iy, imk, imec in zip(x, y, mks, mecs)]
    assert len(res) == nx
    return res
    
    

##======================================================================================================================##
##                                AXES AND PARAM FUNCTIONS                                                              ##
##======================================================================================================================##

def def_plot_params(**new_params):
    """
    Change les parametres de base de matplotlib

    >>> dic=def_plot_params(**{'lines.linewidth': 5.})
    >>> dic = def_plot_params()
    >>> f=pyplot.figure()
    >>> p=pyplot.plot([0, 1], [0, 1])
    >>> a=pyplot.title('rer')
    >>> b=pyplot.xlabel('tertertete')
    >>> show_all(0.1)
    >>> pyplot.close('all')
    """
    res = {'figure.figsize' : (10, 6), 'font.size': 14, 'lines.linewidth': 2., 'lines.markersize': 8} #, 'font.family' : 'DejaVu Serif'
    res.update(new_params)

    for p_key, plt_val in res.items():
        matplotlib.rcParams[p_key] = plt_val

    return res
        
def rescale_axes_from_other(ax, ax_to_rescale, direction="v"):
    """
    for colorbar axes
    """
    if direction == "v":
        newpos = ax.get_position()
        oldpos = ax_to_rescale.get_position()
        new_h = newpos.height
        newymin = newpos.ymin + 0.025 * new_h
        new_h = new_h * 0.95
        newxmin = oldpos.xmin
        new_w = oldpos.width
        newpos = (newxmin, newymin, new_w, new_h)
        ax_to_rescale.set_position(newpos)

    elif direction == "h":
        assert 0, "not implemented"
    else:
        assert 0, 'pb entree demande {0} got {1.__class__} : {1}'.format("h ou v", direction)

    return None

def plot_one_one(**kwarg):
    """
    Plot one one line
    >>> l = plot_one_one()
    >>> show_all(0.1)
    >>> pyplot.close('all')
    """
    argdic = {'color' : "k", 'ls': "-"}
    argdic.update(kwarg)
    
    fig = pyplot.gcf()
    ax = fig.gca()
    
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    lims = [y for lim in (xlim, ylim) for y in lim]
    
    _dnum = 1e-8 #remove autoatatic limits changes
    minval = min(lims) + _dnum
    maxval = max(lims) - _dnum
    
    res = pyplot.plot([minval, maxval], [minval, maxval], **argdic)[0]
    return res

def ax_equal_aspect(method=None, ax=None, **kwarg):
    """
    Rescale x and y axis to have the same spacing
    >>> l = pyplot.plot([0, 3], [0, 1])
    >>> ax_equal_aspect()
    >>> show_all(0.1)
    >>> pyplot.close('all')
    """
    if ax is None:
        ax = pyplot.gcf().gca()

    if method is None:
        ax.set_aspect("equal", **kwarg)
    else:
        xlim = ax.get_xlim()
        ylim = ax.get_ylim()
        xdel = xlim[1] - xlim[0]
        ydel = ylim[1] - ylim[0]
        ratio = xdel / ydel
        #~ print(ratio)
        box = ax.get_position()
        xmin = box.xmin
        ymin = box.ymin
        height = box.height
        width = box.width
        
        if method == "y":
            height = width / ratio
        elif method == "x":
            width = height * ratio
        else:
            assert 0, f'pb input : expected "None, x or y" got {method.__class__} : {method}'

        newpos = (xmin, ymin, width, height)
        #~ print(newpos)
        ax.set_position(newpos)   
            

    return None

def square_plot(lim=None):
    """
    >>> f = pyplot.figure(figsize = (8, 8))
    >>> x = numpy.arange(10)
    >>> y = 2 * x
    >>> l = pyplot.plot(x, y)
    >>> square_plot()
    >>> pyplot.grid(1)
    >>> show_all(0.1)
    >>> pyplot.close('all')
    """
    fig = pyplot.gcf()
    ax = fig.gca()

    if lim is None:
        xlim = ax.get_xlim()
        ylim = ax.get_ylim()
        lim = (min(xlim[0], ylim[0]), max(xlim[1], ylim[1]))
    else:
        assert isinstance(lim, Iterable), f"list type expected got {lim}"
        assert len(lim) == 2, f"2 values expected got {lim}"

    ax.set_xlim(lim)
    ax.set_ylim(lim)
    ax.set_aspect("equal")

    return None

def log_ticks(lims, factors=(1, 2, 5), expand=False):
    """
    >>> all(log_ticks([0.12, 35.4]) == numpy.array([ 0.2,  0.5,  1. ,  2. ,  5. , 10. , 20. ]))
    True
    """
    assert isinstance(lims, Iterable)
    assert len(lims) == 2
    maxval = lims[1]
    minval = lims[0]
    
    
    #~ assert maxval > minval
    minval = math.floor(math.log10(minval))
    # ~ minval = 10 ** minval
    maxval = math.ceil(math.log10(maxval))
    # ~ maxval = 10 ** maxval
    # ~ print(minval, maxval)
    
    nexp = numpy.arange(minval, maxval + 1)
    #~ print nexp
    
    factors = numpy.array(factors, dtype=float)

    res = [10**(int(exp)) * factors for exp in nexp]
    res = numpy.concatenate(res)
    # ~ print(res)
    
    imin = bisect.bisect_left(res, lims[0])
    imax = bisect.bisect_right(res, lims[1])    
    
    # ~ print(imin, imax)
    
    if not expand:
        pass
    else:
        if expand is True or expand=="both":
            expand_lower = True
            expand_upper = True
        else:
            expand_lower = expand == "lower"
            expand_upper = expand == "upper"
            
        # ~ print(expand_lower)
        # ~ print(expand_upper)
        
        lowercond = expand_lower and imin > 0
        imin = imin - int(lowercond)
        imax = imax + int(expand_upper)
    
    # ~ print(imin, imax)
    
    res = res[imin:imax]

    return res

def remove_frames(fig=True, ax=True, axis="on", rm_tklines=False):
    """
    DEPRECATED use bbox in savefig and pad_bbox
    #~ >>> remove_frames()
    #~ >>> show_all(0.1)
    #~ >>> pyplot.close('all')
    """
    print("DEPRECATED, use new utilsality of pyplot.savefig\noptions: bbox_inches='tight', pad_inches=0,transparent=True")
    gcf = pyplot.gcf()
    gca = gcf.gca()
    items = []
    if fig is False:
        pass
    else:
        if fig is True:
            fig = gcf
        items.append(fig)
                    
    if ax is False:
        pass
    else:
        if ax is True:
            ax = gca
        items.append(ax)
    
    for item in items:
        item.patch.set_visible(False)
    
    if axis == "on":
        pass
    else:
        pyplot.axis(axis)
            
    if rm_tklines:
        for yxax in (gca.xaxis, gca.yaxis):
            for il in yxax.get_ticklines():
                il.set_visible(False)
            
    return None

def markerlist(*markers, nmarker):
    """
    Function doc
    >>> markerlist(nmarker = 6)
    ['s', 'o', '<', '>', 'D', 'v']
    >>> markerlist(nmarker = 14)
    ['s', 'o', '<', '>', 'D', 'v', '^', '*', '+', 'x', '1', '2', '3', '4']
    """
    if not markers:
        markers = MARKERS
    elif len(markers) == 1:
        markers = markers[0]
    else:
        pass
            
    size = len(markers)
    assert len(set(markers)) == size, "pb"

    res = [markers[i%size] for i in range(nmarker)]
    return res
    
def linelist(*lines, nline):
    """
    Function doc
    >>> linelist(nline = 4)
    ['-', '--', ':', '-']
    """
    if not lines:
        lines = ["-", "--", ':']
    elif len(lines) == 1:
        lines = lines[0]
    else:
        pass
    
    size = len(lines)
    res = [lines[i%size] for i in range(nline)]
    return res

def colorlist(*colors, ncolor, grey=False):
    """
    Function doc
    >>> colorlist(ncolor = 8)
    ['darkred', 'blue', 'darkgreen', 'purple', 'orange', 'magenta', 'red', 'darkblue']
    >>> colorlist(ncolor = 8, grey = True)
    ['k', '#BBBBBB', '#888888', '#444444', 'k', '#BBBBBB', '#888888', '#444444']
    """
    #~ print(colors)
    if not colors:
        if grey:
            colors = ["k", "#BBBBBB", "#888888", "#444444"]
        else:
            #~ colors = ['darkred', 'darkslateblue', 'darkgreen','darkviolet' ,'darkturquoise','darkgoldenrod']
            colors = ['darkred', "blue", 'darkgreen', 'purple', 'orange', 'magenta', 'red', 'darkblue', 'gold', 'green', 'darkturquoise', 'darkgoldenrod', 'violet', 'chocolate', 'darkviolet']

    elif len(colors) == 1:
        colors = colors[0]
    else:
        pass
    
    size = len(colors)
    res = [colors[i%size] for i in range(ncolor)]
    return res
    

##======================================================================================================================##
##                MARKERSCALE                                                                                           ##
##======================================================================================================================##

class StyleDf(pandas.DataFrame):
    """ Class doc
    >>> sdf = StyleDf(*list(range(3)))
    >>> sdf
         ls marker mec mfc
    0  none      o   k   w
    1  none      o   k   w
    2  none      o   k   w
    >>> sdf.add_property(ls = "-")
      marker mec mfc ls
    0      o   k   w  -
    1      o   k   w  -
    2      o   k   w  -
    >>> sdf.add_property(color = ["b", "g", "r"])
         ls marker mec mfc color
    0  none      o   k   w     b
    1  none      o   k   w     g
    2  none      o   k   w     r

    """
    
    def __init__(self, *args, **kwargs):
        """ Class initialiser """
        if len(args) == 1 and not kwargs:
            obj = args[0]
            assert isinstance(obj, (pandas.DataFrame, StyleDf))
            pandas.DataFrame.__init__(self, obj)
        else:
            if len(kwargs) == 1:
                idx = kwargs.pop("index")
            elif len(args) > 1:
                idx = args
            else:
                raise ValueError("Dont know what to do with {0}\n{1}".format(args, kwargs))
            
            defdic = {"ls" : "none", "mec" : "k", "mfc" : "w", "marker":'o'}
            cols = sorted(defdic.keys())
            pandas.DataFrame.__init__(self, defdic, index=idx, columns=cols)
    
    def add_property(self, *args, **kwargs):
        """
        Add a propoerty
        See class doctest
        """
        
        if len(kwargs) > 1:
            assert not args
            keys = sorted(kwargs.keys())
            for iky, key in enumerate(keys):
                val = kwargs.pop(key)
                dicargs = {key : val}
                if iky == 0:
                    res = self.add_property(**dicargs)
                else:
                    res = res.add_property(**dicargs)
        else:
            #~ print(args, kwargs)
            
            if len(args) == 1 and not kwargs:
                arg = args[0]
                if isinstance(arg, pandas.Series):
                    toadd = arg
                    key = arg.name
                elif arg == "label":
                    toadd = self.labels()
                    key = arg
                else:
                    raise ValueError(f"Dont knwo what to do\n{args}\n{kwargs}")

            elif len(kwargs) == 1:
                assert not args
                
                key = list(kwargs.keys())[0]
                values = kwargs.pop(key)
                
                if isinstance(values, (Number, str)):
                    values = [values] * self.index.size
                else:
                    assert len(values) == self.index.size
                
                toadd = pandas.Series(values, index=self.index, name=key)

            else:
                raise ValueError(f"Dont knwo what to do\n{args}\n{kwargs}")
            
            selcols = [i for i in self.columns if i != key]
            df = pandas.DataFrame(self[selcols])
            newdf = pandas.concat([df, toadd], axis=1)
            res = MarkerScale(newdf)
        
        assert not kwargs
        
        return res


class MarkerScale(StyleDf):
    """ Class doc
    >>> mksle = MarkerScale(*numpy.arange(3))
    >>> mksle
                   ls marker mec mfc
    (-inf, 0.0)  none      o   k   w
    (0.0, 1.0)   none      o   k   w
    (1.0, 2.0)   none      o   k   w
    (2.0, inf)   none      o   k   w
    >>> mksle = mksle.add_property('label')
    >>> mksle = mksle.add_property(ms = [6,8,8,6])
    >>> mksle = mksle.add_property(pandas.Series(["o", "s", "v", "^"], index = mksle.index, name = 'marker'))
    >>> mksle = mksle.add_property(mew = 2)
    >>> mksle = mksle.add_property(mfc = ["w", "k", "w", "k"], mec = "0.5")
    >>> mksle
                   ls      label  ms marker  mew  mec mfc
    (-inf, 0.0)  none     <= 0.0   6      o    2  0.5   w
    (0.0, 1.0)   none  ]0.0 1.0]   8      s    2  0.5   k
    (1.0, 2.0)   none  ]1.0 2.0]   8      v    2  0.5   w
    (2.0, inf)   none      > 2.0   6      ^    2  0.5   k

    >>> mksle.labels()
    (-inf, 0.0)       <= 0.0
    (0.0, 1.0)     ]0.0 1.0]
    (1.0, 2.0)     ]1.0 2.0]
    (2.0, inf)         > 2.0
    Name: label, dtype: object
    
    >>> f=pyplot.figure()
    >>> pls = mksle.plot()
    >>> f.show()
    >>> pyplot.close()
    """
    
    def __init__(self, *args, **kwargs):
        """ Class initialiser
        See class doctest
        """
        #~ print(args, kwargs)
        
        if len(args) == 1 and not kwargs:
            StyleDf.__init__(self, *args)
        else:
            if len(kwargs) == 1:
                limits = kwargs.pop("limits")
            elif len(args) > 1:
                limits = args
            else:
                raise ValueError(f"Dont know what to do with {args}\n{kwargs}")
            
            #~ assert len(mks_kwarg) == (len(limits) + 1), 'pb input, incoherent size, limits must be lower'
            #~ assert all([isinstance(i, (int, float)) for i in limits]), 'exp numb, got {0.__class__}:{0}'.format(limits)
            
            limits = sorted(numpy.array(limits, dtype=float))
            limits = [-numpy.inf] + limits + [numpy.inf]
            idx0 = limits[:-1]
            idx1 = limits[1:] 
            #~ idx = [(i, j) for i,j in zip(idx0, idx1)]
            idx = list(zip(idx0, idx1))
            
            StyleDf.__init__(self, index=idx)
            
    def return_mk(self, value):
        """See class doctest"""
        index = [lim for lim in self.index if (value > lim[0] and value <= lim[1])]
        assert len(index) == 1, f"pb code\nlims={index}"
        df = self.loc[index]
        res = dict(zip(df.columns, df.values[0]))
        return res
            
    def labels(self):
        """See class doctest"""
        labs = []

        for lim in self.index:
            a, b = lim
            #~ print(a,b)
            if a == -numpy.inf:
                text = f"<= {b}"
            elif b == numpy.inf:
                text = f"> {a}"
            else:
                text = f"]{a} {b}]"
            
            labs.append(text)
            
        res = pandas.Series(labs, index=self.index, name="label")
                       
        return res
        
    
        
    def midles(self, add_lims=False):
        """See class doctest"""
        lims = [list(i) for i in self.index]
        
        #change numpy.inf
        lims[0][0] = lims[0][1] - 1
        lims[-1][1] = lims[-1][0] + 1

        lims = pandas.DataFrame(lims, index=self.index)
        mids = lims.mean(axis=1)
        
        if add_lims:
            res = pandas.concat([lims, mids], axis=1)
        else:
            res = mids
        
        return res
    
    def plot(self):
        """See class doctest"""
        #~ argdic = dict(loc = "upper left", numpoints =1)
        #~ argdic.update(kwargs)
        
        x = self.midles()
        x = numpy.sort(x.values.flatten())
        res = {}
        for i in x:
            mkwarg = self.return_mk(i)
            res[i] = pyplot.plot(i, i, **mkwarg)[0]
                
        #~ pyplot.legend(**argdic)
        
        return res


##======================================================================================================================##
##                KWARG PLOT DICT                                                                                       ##
##======================================================================================================================##

class DefPlotKwargs(dict):
    """ 
    Default plot parameter for line and marker
    >>> for k, v in sorted(DefPlotKwargs('l').items()):print(k, v)    
    ls -
    lw 2
    marker 
    >>> for k, v in sorted(DefPlotKwargs('m').items()):print(k, v)
    ls 
    marker o
    mec k
    mew 1
    mfc w
    ms 8
    """
    def __init__(self, typ):
        """ Class initialiser """
        if typ in ("m", "mk", "marker"):
            res = {'mec' : 'k', 'mfc' : "w", 'marker' : "o", "mew" : 1, "ls" : "", "ms" : 8}
        elif typ in ("l", "ln", 'line'):
            res = {'marker' : "", "ls" : "-", "lw" : 2}
        else:
            raise ValueError(f'exp line or marker got {typ}')

        dict.__init__(self, res)

##======================================================================================================================##
##                                MAIN                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    pyplot.close('all')
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--example', default=False, action='store_true')
    parser.add_argument('--colorbar', default=False, action='store_true')
    parser.add_argument('--colormap', default=False, action='store_true')

    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        print("Doctests")
        
        test_result = doctest.testmod()
        print(test_result)
        pyplot.close('all')
    
    #+++++++++++++++++++++++++++++++#
    #    EXAMPLE                    #
    #+++++++++++++++++++++++++++++++#

    if opts.example:
        print("example")
    
        #~ pyplot.figure()
        #~ mnorm = create_boundaries(1.1, 2.5, typ="uniform", factor=0.2, symmetric=1, add_center=False)
        #~ plot_colorbar(cmap="temp", bds=mnorm)
        #~ show_all()
                
        pyplot.figure()
        plot_colorbar(cmap="temp", bds=numpy.arange(20))
        show_all(1)
        
        pyplot.close('all')

        #~ plot_colors()
    
        pyplot.figure()
        plot_colorbar(bds=[-2, 0, 2], cmap=["#FFFFFF", "#FFFFFF", '#000000', '#000000'])

        show_all()
    
    #+++++++++++++++++++++++++++++++#
    #    TEST COLORBAR              #
    #+++++++++++++++++++++++++++++++#
    
    if opts.colorbar:
        pyplot.close('all')
       
        for iextd in (True, False):
            pyplot.figure()
            plot_colorbar(extend=iextd)
            

        show_all()
        
        pyplot.figure()
        cmm = matplotlib.colors.LinearSegmentedColormap.from_list('a', colors=['#FFFF80', '#00FFFF', '#000080', "#FF00FF"])
        #~ cmm=get_colormap("dem", True)
        matplotlib.colorbar.ColorbarBase(ax=pyplot.gcf().gca(), cmap=cmm, extend="both")
        show_all()
        pyplot.figure()
        cmm.set_under("0.2")
        cmm.set_under("0.8")
        matplotlib.colorbar.ColorbarBase(ax=pyplot.gcf().gca(), cmap=cmm, boundaries=[-99, 5, 6, 8, 10, 100, 99], extend='both', spacing='uniform')
        show_all()

    #+++++++++++++++++++++++++++++++#
    #    TESTS                      #
    #+++++++++++++++++++++++++++++++#
    
    if opts.colormap:
        cm1 = get_colormap("jet", 256)
        cm2 = get_colormap("viridis", 256)
        cm1 = get_colormap("jet", 16)
        cm2 = get_colormap("viridis", 16)
        
        plot_style()
