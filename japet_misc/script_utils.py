"""
Utils for automtic utils
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os
import glob
import argparse
import doctest

import string

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

_fundir = os.path.dirname(__file__)

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def write_auto_sh(sh_path, dirname, **kwargs):
    """write an sh file with"""
    scripts = get_py_scripts(dirname=dirname, **kwargs)
    towrite = "\n".join(scripts)

    with open(sh_path, "w", encoding="utf8") as fw:
        fw.write(towrite)
        
    return None

def list_scripts(dirname):
    """List python scrits in a directory
    """
    res = sorted(glob.glob(f"{dirname}/*.py"))
    return res

def get_py_scripts(*options, dirname, max_letter=None, prefix="python3"):
    """print python script for automatic sh"""
    scripts = list_scripts(dirname=dirname)
    if max_letter is not None:
        letters = [let for let in string.ascii_lowercase if let <= max_letter]
        scripts = [ifile for ifile in scripts if os.path.basename(ifile)[0] in letters]
    #
    optstr = " ".join(options)
    res = [f"{prefix} {ifile} {optstr}" for ifile in scripts]
    
    return res
    
    
    
    
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
